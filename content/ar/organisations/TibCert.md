---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: من الاثنين إلى الخميس, GMT+5.30
response_time: ساعتين في أوقات العمل و&nbsp;24 ساعة في غيرها
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

فريق الطوارئ الحاسوبية التِّبِتِي يسعى إلى بناء كيان ائتلافي مهمته تقليل المخاطر السبرانية و&nbsp;التصدّي لها في المجتمع التبتي، و&nbsp;كذلك إلى زيادة القدرات البحثية التقنية لدى التِّبِتِيِّين في التهديدات في المهجر و&nbsp;المراقبة و&nbsp;الحجب في داخل التِّبِت بهدف تحقيق حرية أكبر و&nbsp;أمان أكثر للمجتمع التبتي كلِّه.

تتضمن مهّمة تِبسِرت:
- إيجاد منصّة للتعاون طويل الأمد ما بين أصحاب المصلحة في المجتمع التبتي فيما يتعلّق بمسائل الأمان السبراني و&nbsp;متطلّباته،
- تعميق الصلات و&nbsp;تطوير صيرورة نظامية للتعاون ما بين التبتيين و&nbsp;الباحثين في مجال البرمجيات الخبيثة و&nbsp;الأمان السبراني لضمان الإفادة المتبادَلة،
- زيادة المصادر المتاحة للتبتيين للدفاع ضد الهجمات السبرانية و&nbsp;التصدّي لها بطريق نشر المعلومات دوريًا و&nbsp;التوصيات بشأن التهديدات التي يواجهها التبتيون،
- مساعدة التبتيين في التبت على تفادي الحجب و&nbsp;المراقبة بإمدادهم دوريا بالمعلومات التفصيلية و&nbsp;التحليلات و&nbsp;كذلك الحلول المقترحة.

