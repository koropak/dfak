---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 days
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

The Digital Defenders Partnership offers support to human rights defenders under digital threat, and works to strengthen local rapid response networks. DDP coordinates emergency support for individuals and organizations such as human rights defenders, journalists, civil society activists, and bloggers.

DDP has five different types of funding that address urgent emergency situations, as well as longer-term grants focused on building capacity within an organization. Furthermore, they coordinate a Digital Integrity Fellowship where organizations receive personalized digital security and privacy trainings, and a Rapid Response Network program.
