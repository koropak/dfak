---
layout: sidebar.pug
title: "Alguien que conozco fue arrestado/a"
author: Peter Steudtner, Shakeeb Al-Jabri
language: es
summary: "un amigo, colega o familiar ha sido detenido/a por fuerzas de seguridad. Quisiera limitar el impacto del arresto en ellos y cualquier otro que pueda estar implicado/a."
date: 2019-03-13
permalink: /es/arrested/
parent: Home
sidebar: >
  <h3>Lee más sobre qué hacer si alguien que conoces ha sido arrestado/a:</h3>

  <ul>
    <li><a href="https://coping-with-prison.org">Inspiración y orientación para personas encarceladas y sus familias, abogados y simpatizantes</a></li>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Obtener ayuda para hacer campaña en favor de personas detenidas</a></li>
  </ul>
---

# Alguien que conozco fue arrestado/a

El arresto de defensores de derechos humanos, periodistas y activistas ponen a estas personas y a aquellos con los que trabajan en un gran riesgo.

Esta guía está orientada especialmente a países con carencias en términos de derechos humanos y debido proceso, o en donde las autoridades pueden sortear los caminos legales, o en donde actores no estatales operan con libertad. En estos casos los arrestos representan un gran riesgo para las victimas, sus colegas y familiares.

En esta guía apuntamos a aliviar el peligro que estas personas enfrentan y a limitar el acceso a quienes los arrestaron a información sensible que pueda incriminar a las víctimas y sus colegas, o que puede ser usada para comprometer otras operaciones.

Cuidar a quienes han sido detenidos así como también vigilar los impactos de la detención en términos digitales puede ser agotador y retador. Intenta buscar el apoyo de otras personas y coordina tus acciones con la comunidad afectada.

También es importante que te cuides a ti mismo/a y a otros afectados por este arresto:

- [cuidando tus necesidades psico-sociales y tu bienestar](../self-care),
- [cuidando el lado legal de la ayuda que brindas](../support)
- [obteniendo ayuda para hacer campaña en favor de aquellos detenidos/as](https://www.newtactics.org/search/solr/arrest)

Si te sientes abrumado/a en términos técnicos o emocionales o (por la razón que sea) no te sientes en posición de seguir los pasos descritos a continuación, por favor busca ayuda y guía con las organizaciones listadas [acá](../support) que ofrecen evaluaciones iniciales entre sus servicios.

## Elabora un plan

Antes de actuar con la información de las secciones siguientes, por favor sigue estos pasos:

- Intenta leer la guía completa y construye un panorama de todas las áreas de impacto importantes antes de tomar acciones en aspectos específicos. La razón de esto es porque las diferentes secciones se enfocan en diferentes escenarios de riesgo que pueden solaparse, por lo que neesitarías construir tu propia secuencia de acciones.
- Tómate un tiempo con amigos o con tu equipo para hacer los diferentes análisis de riesgo que son necesarios.

<a name="harm-reduction"></a>
## Prepárate antes de actuar

¿Tienes razones para creer que este arresto puede llevar a repercusiones para los familiares, amigos o colegas incluyéndote?

Esta guía te llevará por una serie de pasos para proveerte de soluciones que puedan ayudar a reducir la exposición de las personas detenidas y de cualquiera que esté involucrado.

**Consejos para una respuesta coordinada**:

En todas las situaciones en donde una persona ha sido detenida, la primera cosa que debemos notar es que frecuentemente cuando ocurren este tipo de incidentes, muchos amigos o colagas reaccionan al momento, resultando en esfuerzos duplicados e incluso contradictorios. Por lo que es importante recordar que las acciones coordinadas y concertadas, tanto a nivel local como internacional, son necesarias para ayudar a quien está detenido/a y para ayudar a sus redes de apoyo, familia y amigos.

- Establece un equipo de crisis que coordinará todas las actividades de apoyo, cuidado, campaña, etc.
- Involucra a familiares, pareja, etc. tanto como sea posible (respetando susn límites si, por ejemplo, están sobrecargados)
- Establece metas claras para tu campaña de apoyo (y revísalas frecuentemente): por ejemplo, puedes plantear como una meta la liberación de la persona detenida, garantizar su bienestar o proteger a su familia y seguidores, y asegurar su bienestar como la meta más inmediata.
- Acuerda usar canales seguros de comunicación así como frecuencia y limites (por ejemplo que no haya comunicaciones entre 10pm y 8am excepto para emergencia o noticias de última hora).
- Distribuye tareas entre los miembros del equipo y busca ayuda con terceros (análisis, incidencia, trabajo de medios, documentación, etc.).
- Pide más ayuda fuera del "equipo de crisis" para cosas desde comidascompras hasta necesidades básicas (por ejemplo comidas regulares, etc.).

**Precauciones de seguridad digital**

Si tienes razones para temer repercusiones para ti u otros seguidores, antes de lidiar con cualquier emergencia digital conectada a la detención, es crucial que tomes algunas medidas preventivas a nivel de seguridad digital para protegerte a ti y a otros de amenazas inmediatas.

- Acuerda cuáles serán los canales de comunicación (seguros) que tu y tu red usareis para coordinar la mitigación de la emergencia y comunicarse sobre la persona detenida y repercusiones posteriores.
- Reduce la información en tus dispositivos al mínimo necesario y protege los datos en tus dispositivos con [cifrado](https://ssd.eff.org/es/module/manteniendo-sus-datos-seguros).
- Crea [copias de seguridad seguras y cifradas](https://helplinedocs.accessnow.org/182-Secure_Backup.html) de toda tu información y mantenlas en un lugar que no sea encontrado durante redadas o potenciales detenciones adicionales.
- Comparte las contraseñas de dispositivos, cuentas de internet, etc. con una persona de confianza que no esté bajo peligro inmediato.
- Acuerda acciones a ser tomadas como primera respuesta ante tu posible detención, como suspensión de cuentas, borrado remoto de dispositivos, etc.

Esta página te guiará por una serie de pasos para encontrar soluciones que puedan ayudar a reducir la exposición de la persona detenida y de cualquier otra relacionada a esta, por ejemplo porque la información de contactos no se pueda encontrar en los dispositivos, cuentas de redes sociale o correos electrónicos de la persona detenida.

## Análisis de riesgo
### Reducir el daño potencial causado por tus propias acciones

En general, deberías intentar de basar tus acciones en esta pregunta:

- ¿Cuáles son los impactos de las acciones únicas y combinadas de la persona detenida, pero también de sus comunidades, colegas activistas, amigos, familia, etc. incluyendote?

Cada una de las siguientes secciones describirán aspectos especiales de este análisis de riesgo.

Algunas consideraciones básicas son:

- Antes de borrar cuentas, datos, hilos de redes sociales, etc., asegúrate de que tienes documentada la información que se está borrando, especialmente si se tendrá que restaurar esa información o la necesitarás como futura evidencia.
- Si borras o eliminas cuentas o archivos, ten en cuenta que:
    - Las autoridades pueden interpretar esto como destrucción de evidencia.
    - Esto puede hacer más difícil la situación de la persona detenida en el caso en que las autoridades buscan el acceso a las cuentas y no lo consigan, haciendo ver a la persona detenida como no creible y pudiendo resultar en acciones negativas producto de la eliminación de las cuentas.
- Si informas a otros que su información personal está almacenada en dispositivos o cuentas tomadas por las autoridades y estas comunicaciones son interceptadas, pueden representar evidencia adicional de la relación con la persona detenida.
- Los cambios en los procedimientos de comunicación (incluyendo el borrado de cuentas, etc.) pueden atraer la atención de las autoridades.

### Informando a los contactos

En general, es imposible determinar si las autoridades encargadas de la detención tienen la capacidad de mapear la red de contactos de la persona detenida, y si lo han hecho o no. Así que tenemos que asumir el peor escenario, que es que o ya lo hicieron o lo van a hacer.

Antes de comenzar a informar a la red de contactos de la persona detenida, por favor evalúa el riesgo de informarlos:

- ¿Tienes una copia de la red de contactos de la persona detenida? ¿Puedes chequear quién está en su lista de contactos tanto en sus dispositivos, cuentas de correo y plataformas de redes sociales? Compila una lista de posibles contactos para tener un panorama de quiénes pudieran estar afactados.
- ¿Existe un riesgo de que informar a los contactos los pueda relacionar más cercanamente con la persona detenida y esto pueda ser usado por las autoridades en contra de ellos?
- ¿Deberían ser informados todos los contactos o solo un grupo específico?
- ¿Quién informará a los contactos? ¿Quién ya esta en contacto con ellos? ¿Cuál es el impacto de esta decisión?
- Establece el canal más seguro de comunicación, incluyendo reuniones físicas en espacios en donde no haya vigilancia, por ejemplo mediante circuito cerrado de televisión (CCTV), para informar a los contactos implicados.

### Documentar para preservar evidencia

Antes de borrar cualquier contenido de sitios web, redes sociales, hilos, etc., puedes querer asegurarte de tenerlos documentado de antemano. Una razón para documentar sería capturar cualquier señal o prueba de que las cuentas han sido infiltradas - cómo contenidos agregados o impersonados - o para conservar contenidos que puedas necesitar como evidencia legal.

Dependiendo del sitio web o plataforma de redes sociales desde donde quieras documentar información, se pueden tomar diferentes vías:

- Puedes tomar capturas de pantalla de partes relevantes (asegúrate de que aparezcan horas de publicación, URLs, etc.).
- Puedes chequear que sitios web o blogs relevantes están indexados en [Wayback Machine](https://archive.org/web), o descargar estas páginas o blogs en tu computadora.

*Recuerda que es importante mantener la información que descargues en un dispositivo seguro almacenado en un lugar seguro.*

### Incautación de dispositivos

Si cualquier dispositivo de la persona detenida fue confiscado durante o después del arresto, por favor lee la guia [Perdí mis dispositivos](../topics/lost-device), en particular la sección de [borrado remoto](../topics/lost-device/questions/find-erase-device), incluyendo recomendaciones en caso de detección.


### Datos en línea y cuentas incriminatorias

Si la persona detenida tiene información en sus dispositivos que pueda hacerle daño a ella o a otras, puede ser una buena idea intentar limitar el acceso a esta información a quienes realizaron la detención.

Antes de hacer esto, compara el riesgo asociado a esta información con el riesgo asociado a la molestia de las fuerzas de seguridad al no tener acceso a la información (o a la acción legal que pueden tomar basándose en destrucción de evidencia). Si el riesgo asociado a la información es más alto, puedes proceder a borrar información relevante o cerrando/suspendiendo y desvinculando cuentas con las instrucciones a continuación:

#### Suspender o cerrar cuentas

Si tienes acceso a las cuentas que deseas cerrar, puedes seguir el proceso para cada una de ellas. ¡Por favor asegúrate de tener una copia de seguridad del contenido borrado! Toma en cuenta que después de cerrar una cuenta el contenido no será inmediatamente inaccesible: en el caso de Facebook, por ejemplo, puede tomar hasta dos semanas para que el contenido sea borrado de todos los servidores.

Si no tienes acceso a las cuentas de la persona detenida o necesitas una acción más urgente sobre las cuentas de redes sociales por favor busca ayuda en las organizaciones listadas [aquí](../support) que pueden ofrecerte ayuda con cuentas entre sus servicios.

#### Desvincula cuentas de dispositivos

Algunas veces, es posible que desees desvincular tu cuenta de ciertos dispositivos, ya que este enlace puede dar acceso a datos confidenciales
a cualquier persona que controle dichos dispositivos. Para hacer esto,
puedes seguir [las instrucciones en el flujo de trabajo "Perdí mi dispositivo"](../topics/lost-device/questions/accounts)

No olvides desvincular [cuentas de banco](#online_bank_accounts) de los dispositivos.

#### Cambia las contraseñas

Si decides no cerrar o suspender cuentas, puede ser útil cambiar sus contraseñas siguiendo [las instrucciones del flujo de trabajo "Perdí mi dispositivo"](../topics/lost-device/questions/passwords).

Considera también habilitar la autenticación en 2 factores para
aumentar la seguridad de las cuentas de la persona detenida siguiendo [las instrucciones en el flujo de trabajo "Perdí mi dispositivo"](../topics/lost-device/questions/2fa).

En el caso de que la misma contraseña estaba siendo usada en diferentes cuentas, deberías cambiarlas todas ya que también pueden estar comprometidas.

**Otros consejos sobre el cambio de contraseñas**

- usa un administrador de contraseñas (como [KeepassXC](https://keepassxc.org)) para documentar las contraseñas cambiadas para su futuro uso o para entregárselas a la persona detenida luego de su liberación.
- Asegúrate de contarle a la persona detenida, como muy tarde justo después de su liberación, acerca de las contraseñas cambiadas y devuélvele la posesión de sus cuentas.

#### Elimina las membresías de grupos y des-comparte carpetas

Si la persona detenida está en grupos como por ejemplo de Facebook, WhatsApp, Signal, o Wire, o pueden acceder a carpetas compartidas en línea, y su presencia en estos grupos le da a la persona detenida acceso a información privilegiada y potencialmente peligrosa, puedes querer sacarla de los grupos y de las carpetas u otros espacios compartidos.

**Instrucciones para quitar membresías de grupo en diferentes servicios de mensajería instantánea:**

- [WhatsApp](https://faq.whatsapp.com/en/android/26000116/?category=5245251&lang=es)
- Telegram - La persona que creó el grupo puede eliminar participantes seleccionando "Información del grupo" y deslizando a la izquierda a aquellos usuarios que desee eliminar.
- Wire
    - [Instruciones para móvil](https://support.wire.com/hc/en-us/articles/203526410)
    - [Instruciones para la aplicación de escritorio](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- Signal - No puedes eliminar participangtes de grupos de Signal, lo que puedes hacer es crear un nuevo grupo excluyendo la cuenta de la persona detenida.

**Instrucciones para des-compartir carpetas en diferentes servicios ne línea:**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- Google Drive - Primero busca los archivos de interés usando el operador de búsqueda "to:" (`to:usario@gmail.com`), luego selecciona todos los resultados y has clic en el ícono de compartir. Clic en Avanzado, elimina la dirección de correo de la ventana que aparece y guardar.
- [Dropbox](https://help.dropbox.com/es-es/files-folders/share/unshare-folder)
- [iCloud](https://support.apple.com/es-lamr/HT201081)


### Borrado de publicaciones

En algunos casos querrás remover contenidos de las líneas de tiempo en las cuentas de redes sociales de la persona detenida u otras publicaciones asociadas a sus cuentas, ya que pueden ser usadas como evidencia en contra de la persona detenida o para generar confusión y conflicto dentro de su comunidad o desacreditarle.

Algunos servicios permiten borrar líneas de tiempo y publicaciones de formas más sencillas que otros. Guías para Twitter, Facebook e Instagram están disponibles abajo. Por favor asegúrate de que has documentado el contenido que quieres borrar, en el caso de necesitarlo como evidencia en caso de manipulación, etc.

- Para Twitter, puedes usar [Tweet Deleters](https://tweetdeleter.com/es/home).
- Para Facebook, puedes seguir [la guía de Louis Barclays "How to delete your Facebook News Feed" (En Inglés)](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), basada en la aplicación para el navegador Chrome llamada [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Para Instagram, puedes seguir [las instrucciones en la página de Instagram sobre la edición y eliminación de publicaciones](https://help.instagram.com/997924900322403) (pero ten en cuenta que las personas con acceso a la configuración de tu
cuenta podrán ver el historial de la mayoría de tus interacciones incluso después de eliminar el contenido).


### Elimina asociaciones en línea riesgosas

Si existe cualquier información en línea en donde la persona detenida es nombrada y que pueda tener implicaciones negativas para esta o sus contactos, es una buena idea borrarla si esto no perjudica más a la persona detenida.

- Has una lista de los espacios en línea y la información que necesitaría ser removida o cambiada.
- Si has identificado contenido para ser eliminado o cambiado, puedes querer hacer una copia de seguridad de este antes de proceder con su eliminación o solicitudes de bajada de información.
- Evalúa si la eliminación del nombre de la persona detenida puede tener algún impacto negativo en su situación (por ejemplo, al eliminar su nombre de una lista de miembros de una organización puede proteger a esta última, pero puede también dejar a la persona detenida sin justificación de que estaba trabajando para esa organización).
- Si tienes acceso a los respectivos sitios o cuentas, cambia o elimina la información sensible.
- Si no tienes acceso, pídele a personas que lo tengan que cambien o eliminen la información sensible.
- Puedes encontrar instrucciones para eliminar contenidos en servicios de Google [aquí](https://support.google.com/webmasters/answer/6332384?hl=es#get_info_off_web)
- Chequea si los sitios que contienen información sensible han sido indexados por Wayback Machine o Google Cache. Si es el caso, este contenido debe ser eliminado de igual manera.

<a name="online_bank_accounts"></a>
### Cuentas de bancos

Muy frecuentemente, las cuentas de banco pueden ser manejadas en línea, y una verificación vía dispositivos móviles puede ser necesaria para realizar transacciones o incluso solamente para acceder a la cuenta. Si una persona detenida no está accediendo a sus cuentas bancarias por un período largo de tiempo, esto puede tener implicaciones sobre la situación financiera de la persona y su posibilidad de acceder a la cuenta posteriormente. En estos casos asegúrate de:

- Desvincular los dispositivos incautados de la persona detenida de sus cuentas bancarias.
- Obtén una autorización y poder legal de la persona detenida para poder operar sus cuentas bancarias en su lugar al comienzo del proceso (en coordinación con sus familiares).

## Consejos finales

- Asegúrate de devolver todos los datos respaldados y cuentas a la persona detenida luego de su liberación.
- Lee [los consejos del flujo de diagnóstico "Perdí mi dispositivo"](../topics/lost-device/questions/device-returned) sobre cómo tratar los dispositivos incautados después de que las autoridades los devuelvan.
