---
name: Deflect
website: https://www.deflect.ca
logo:
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: lunes-viernes, 24/5, UTC-4
response_time: 6 horas
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect es un servicio gratuito de seguridad para sitios web que protege a la sociedad civil y grupos de derechos humanos de ataques digitales.
