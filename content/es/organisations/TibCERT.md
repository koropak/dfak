---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: Lunes - Viernes, GMT+5.30
response_time: 24 horas durante jornadas no laborales, 2 horas durante nuestro horario de trabajo.
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

El Equipo Tibetano de Preparación para Emergencias Informáticas (TibCERT) busca crear una estructura formal basada en una coalición para reducir y mitigar las amenazas en línea en la comunidad tibetana, y ampliar la capacidad de investigación técnica de los tibetanos sobre las amenazas en la diáspora, la vigilancia y la censura dentro del Tibet. Asegurando en última instancia mayor libertad y seguridad en línea para la sociedad tibetana como un conjunto.

La mission de TibCERT incluye:

- Crear y mantener una plataforma para la colaboración a largo plazo entre partes interesadas de la comunidad tibetana en cuestiones de necesidades y seguridad digital.
- Profundizar conexiones y desarrollar un proceso formal de colaboración entre los tibetanos y los investigadores mundiales de malware y ciberseguridad para asegurar un intercambio de beneficio mutuo.
- Aumentar los recursos disponibles entre los tibetanos para defenderse y mitigar los ataques en línea mediante la publicación periódica de información y generar recomendaciones sobre las amenazas que enfrenta la comunidad.
- Ayudar a los tibetanos en el Tíbet a eludir la censura y la vigilancia proporcionando información, análisis periódicos detallados, y posibles soluciones.
