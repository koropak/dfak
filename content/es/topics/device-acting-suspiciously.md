---
layout: topic
title: "Mi dispositivo está actuando de forma sospechosa"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: es
summary: "Si su computadora o teléfono actúa de manera sospechosa, puede haber software no deseado o malicioso en su dispositivo."
date: 2019-08
permalink: /es/topics/device-acting-suspiciously
parent: /es/
---

# Mi dispositivo está actuando de forma sospechosa

Los ataques de malware han evolucionado y se han vuelto muy sofisticados con los años. Estos ataques representan múltiples amenazas diferentes y pueden tener serias implicaciones a nivel de infraestructura, datos personales y organizacionales.

Los ataques de malware vienen en diferentes formas, como virus, phishing, ransomware, troyanos y rootkits. Algunas de las amenazas son: fallas en los equipos, robo de datos (credenciales confidenciales de la cuenta, información financiera, inicios de sesión de cuentas bancarias), un atacante que les chantajea para pagar un rescate al tomar el control de su dispositivo, y/o la toma de control de su dispositivo y su uso para lanzar ataques de Denegación Distribuida de Servicio o DDoS.

Algunos métodos utilizados comúnmente por los atacantes para comprometerte a ti y tus dispositivos pueden parecen actividades regulares, como:

- Un correo electrónico o una publicación en las redes sociales que tentarán los usuarios en abrir un archivo adjunto o hacer clic en un enlace.

- Impulsar a las personas a descargar e instalar software desde una fuente no confiable.

- Presionar a alguien para que ingrese su nombre de usuario y contraseña en un sitio web cuyo aspecto parece legítimo, pero no lo es.

Esta sección del kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para determinar si tu dispositivo está probablemente infectado o no.

Si crees que tu computadora o dispositivo móvil ha comenzado a actuar de manera sospechosa, primero debes pensar en cuáles son los síntomas.

Los síntomas que comúnmente se pueden interpretar como actividad sospechosa del dispositivo, pero a menudo no son motivo suficiente para preocuparse, incluyen:

- Sonido de cliqueos durante llamadas telefónicas.
- Gasto de batería inesperado.
- Recalentamiento mientras el dispositivo no está en uso.
- Funcionamiento lento del dispositivo.

Estos síntomas a menudo se consideran erroneamente como indicadores confiables de actividad maliciosa en el dispositivo. Sin embargo, cualquiera de ellos por sí solos no es motivo suficiente para preocuparse.

Los síntomas más confiables de un dispositivo comprometido por lo general son:

- El dispositivo se reinicia frecuentemente por sí solo.
- Las aplicaciones fallan, especialmente después de la acción de entrada.
- Las actualizaciones del sistema operativo y/o los parches de seguridad fallan repetidamente.
- La luz indicadora de actividad de la cámara web está encendida mientras la cámara web no está en uso.
- ["Pantallas azules de la muerte"](https://es.wikipedia.org/wiki/Pantalla_azul_de_la_muerte) o pánicos del kernel de forma muy repetida.
- Ventanas con intermitencias.
- Advertencias de antivirus.

## Workflow

### start

Dada la información proporcionada en la introducción, si aún sientes que tu dispositivo puede estar comprometido, la siguiente guía puede ayudarte a identificar el problema.

- [Creo que mi dispositivo móvil está actuando de manera sospechosa](#phone-intro)
- [Creo que mi computadora está actuando de manera sospechosa](#computer-intro)
- [Ya no siento que mi dispositivo pueda estar comprometido](#device-clean)

### device-clean

> ¡Genial! Sin embargo, ten en cuenta que estas instrucciones te ayudarán a realizar solo un análisis rápido. Si bien debería ser suficiente para identificar anomalías visibles, un spyware más sofisticado podría ser capaz de esconderse de una manera más efectiva.

Si aún sospechas que el dispositivo podría estar comprometido, es posible que desees:

- [Buscar ayuda adicional](#malware_end)
- [Proceder directamente a un reinicio del dispositivo](#reset).

### phone-intro

> Es importante tener en cuenta cómo tu dispositivo pudo haberse comprometido.
>
> - ¿Hace cuánto tiempo comenzaste a sospechar que tu dispositivo estaba actuando de manera sospechosa?
> - ¿Recuerdas haber hecho clic en algún enlace de fuentes desconocidas?
> - ¿Has recibido mensajes de personas que no reconoces?
> - ¿Instalaste algún software sin firmar?
> - ¿El dispositivo ha estado fuera de tu posesión?

Reflexiona sobre estas preguntas para intentar identificar las circunstancias, si es que las hubiera, que llevaron a que tu dispositivo se vea comprometido.

- [Tengo un dispositivo Android](#android-intro)
- [Tengo un dispositivo iOS](#ios-intro)

### android-intro

> Primero verifica si hay aplicaciones desconocidas instaladas en tu dispositivo Android.
>
> Puedes encontrar una lista de aplicaciones en la sección "Aplicaciones" del menú de configuración. Identifica cualquier aplicación que no venga preinstalada con tu dispositivo y que no recuerdes haber descargado.
>
> Si sospechas de alguna de las aplicaciones en la lista, realiza una búsqueda en la web y busca recursos para ver si hay informes que identifiquen a la aplicación como maliciosa.

¿Encontraste alguna aplicación sospechosa?

- [No, no lo hice](#android-unsafe-settings)
- [Sí, identifiqué aplicaciones potencialmente maliciosas](#android-badend)

### android-unsafe-settings

> Android les da la opción a los usuarios de controlar aspectos avanzados del dispositivo. Esto puede ser útil para los desarrolladores de software, pero también puede exponer los dispositivos a ataques adicionales. Debes revisar estos ajustes de seguridad y asegurarte de que estén configurados con las opciones más seguras. Los fabricantes pueden enviar dispositivos con valores inseguros por defecto. Por lo que estas configuraciones deben revisarse incluso si no has hecho ningún cambio en tu dispositivo.
>
> #### Aplicaciones no firmadas
>
> Android normalmente bloquea la instalación de aplicaciones que no se cargan desde la tienda de aplicaciones de Google (Google Play Store). Google tiene procesos para revisar e identificar aplicaciones maliciosas en esta tienda. Los atacantes a menudo intentan evitar estas comprobaciones al enviar aplicaciones maliciosas directamente al usuario al compartir un enlace o un archivo con ellos. Es importante asegurarse de que tu dispositivo no permita la instalación de aplicaciones desde fuentes no confiables.
>
> Revisa la sección "Seguridad" de la configuración de Android y asegúrate de que la opción Instalar aplicaciones desde origenes desconocidos esté deshabilitada.
>
> #### Modo de desarrollador y acceso ADB (Android Debug Bridge)
>
> Android permite a los desarrolladores ejecutar comandos directamente en el sistema operativo mientras se encuentra en "Modo de desarrollador". Cuando está habilitado, esto expone a los dispositivos a ataques físicos. Alguien con acceso físico al dispositivo podría usar el modo Desarrollador para descargar copias de datos privados del dispositivo o para instalar aplicaciones maliciosas.
>
> Si ves un menú del modo Desarrollador en la configuración de tu dispositivo, debes asegurarse de que el acceso mediante ADB esté deshabilitado.
>
> #### Google Play Protect
>
> El servicio Google Play Protect está disponible en todos los dispositivos Android recientes. Este realiza análisis regulares de todas las aplicaciones instaladas en tu dispositivo. Play Protect también puede eliminar automáticamente cualquier aplicación maliciosa conocida de tu dispositivo. Al activar este servicio se envía a Google información sobre tu dispositivo.
>
> Google Play Protect se puede habilitar en la configuración de seguridad de tu dispositivo. Hay más información disponible en el sitio [Play Protect](https://www.android.com/intl/es_es/play-protect/).

¿Identificaste alguna configuración insegura?

- [No, no lo hice](#android-bootloader)
- [Sí, identifiqué configuraciones potencialmente inseguras](#android-badend)

### android-bootloader

> El bootloader de Android es un software clave que se ejecuta tan pronto como enciendes tu dispositivo. Es un gestor de arranque que permite al sistema operativo iniciar y utilizar el hardware correspondiente. Un bootloader comprometido le da a un atacante acceso completo al hardware del dispositivo. La mayoría de los fabricantes envían sus dispositivos con el bootloader bloqueado. Una forma común de identificar si el bootloader firmado por el fabricante ha sido modificado es reiniciar tu dispositivo y buscar el logotipo de arranque. Si aparece un triángulo amarillo con un signo de exclamación, entonces el bootloader original fue reemplazado. Tu dispositivo también puede verse comprometido si muestra una pantalla de advertencia de bootloader desbloqueado y no lo habías desbloqueado previamente para instalar una ROM de Android personalizada como CyanogenMod. Deberías realizar un restablecimiento de fábrica de tu dispositivo si muestra una pantalla de advertencia de bootloader desbloqueada que no esperas.

¿Está el bootloader comprometido o su dispositivo está usando el bootloader original?

- [El bootloader de mi dispositivo está comprometido](#android-badend)
- [Mi dispositivo está usando su bootloader original](#android-goodend)

### android-goodend

> No parece que tu dispositivo haya sido comprometido.

¿Sigues preocupado de que tu dispositivo esté comprometido?

- [Sí, me gustaría buscar ayuda profesional](#malware_end)
- [No, he resuelto mis problemas](#resolved_end)

### android-badend

> Tu dispositivo puede estar comprometido. Un [restablecimiento de fábrica](#reset) probablemente eliminará cualquier amenaza que esté presente en tu dispositivo. Sin embargo, no siempre es la mejor solución. Además, es posible que desees investigar más a fondo esta alternativa para identificar tu nivel de exposición y la naturaleza precisa del ataque.
>
> Es posible que desees utilizar una herramienta para el autodiagnóstico llamada [VPN de emergencia](https://www.civilsphereproject.org/emergency-vpn), o buscar ayuda de una organización que pueda asistirte.

¿Te gustaría buscar más ayuda?

- [Sí, me gustaría buscar ayuda profesional](#malware_end)
- [No, tengo una red de asistencia local a la que puedo contactar](#resolved_end)

### ios-intro

> Comprueba la configuración de iOS para ver si hay algo inusual.
>
> En la aplicación Configuración, verifica que tu dispositivo esté vinculado a tu ID de Apple. El primer elemento del menú en el lado izquierdo debe ser tu nombre o el nombre que usas para tu ID de Apple. Haz clic en él y verifica que se muestre la dirección de correo electrónico correcta. En la parte inferior de esta página, verás una lista con los nombres y modelos de todos los dispositivos iOS vinculados a este ID de Apple.

- [Toda la información es correcta y todavía tengo el control de mi ID de Apple](#ios-goodend)
- [El nombre u otros detalles son incorrectos o veo dispositivos en la lista que no son míos](#ios-badend)

### ios-goodend

> No parece que tu dispositivo haya sido comprometido.

¿Sigues preocupado de que tu dispositivo esté comprometido?

- [Sí, me gustaría buscar ayuda profesional](#malware_end)
- [No, he resuelto mis problemas](#resolved_end)

### ios-badend

> Tu dispositivo puede estar comprometido. Un restablecimiento de fábrica probablemente eliminará cualquier amenaza que esté presente en tu dispositivo. Sin embargo, no siempre es la mejor solución. Además, es posible que desees investigar más a fondo esta alternativa para identificar tu nivel de exposición y la naturaleza precisa del ataque.
>
> Es posible que desees utilizar una herramienta para el autodiagnóstico llamada [VPN de emergencia](https://www.civilsphereproject.org/emergency-vpn), o buscar ayuda de una organización que pueda asistirte.

¿Te gustaría buscar más ayuda?

- [Sí, me gustaría buscar ayuda profesional](#malware_end)
- [No, tengo una red de asistencia local a la que puedo contactar](#resolved_end)

### computer-intro

> **Nota: En el caso de que estés bajo un ataque de ransomware, ve directamente a [No More Ransom!](https://www.nomoreransom.org/es/index.html).**
>
> Este flujo de trabajo te ayudará a investigar actividades sospechosas en tu computadora. Si estás ayudando a una persona de forma remota, puedes intentar seguir los pasos descritos en los enlaces a continuación utilizando un software de escritorio remoto como TeamViewer, o puedes revisar marcos de trabajo forenses remotos como el [GRR Rapid Response](https://github.com/google/grr). Ten en cuenta que la latencia y la confiabilidad de la red serán esenciales para llevar a cabo este proceso correctamente.

Por favor selecciona tu sistema operativo:

- [Tengo una computadora con Windows](#windows-intro)
- [Tengo una computadora Mac](#mac-intro)
- [Tengo una computadora con Linux](#linux-intro)

### windows-intro

> Puedes seguir esta guía introductoria para investigar actividades sospechosas en dispositivos con Windows:
>
> - [How to Live Forensic on Windows por Te-k (En Inglés)](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

¿Te ayudaron estas instrucciones a identificar alguna actividad maliciosa?

- [Sí, creo que la computadora está infectada](#device-infected)
- [No, no se identificó ninguna actividad maliciosa](#device-clean)

### mac-intro

> Para identificar una posible infección en una computadora Mac, debes seguir estos pasos:
>
> 1. Comprobar si hay programas sospechosos que se inician automáticamente
> 2. Comprobar si hay procesos en ejecución sospechosos
> 3. Comprobar si hay extensiones sospechosas del kernel
>
> El sitio web [Objective-See (En Inglés)](https://objective-see.com) proporciona varias utilidades gratuitas que facilitan este proceso:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) se puede usar para identificar todos los programas que están registrados para iniciarse de forma automática.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) puede ser usado para verificar procesos en ejecución e identificar aquellos que parecen sospechosos (por ejemplo, porque no están firmados, o porque están marcados por VirusTotal) .
> - [KextViewr](https://objective-see.com/products/kextviewr.html) puede ser usado para identificar cualquier extensión sospechosa del kernel que se carga en la computadora Mac.
>
> En caso de que no revelen nada sospechoso de inmediato y deseas realizar un seguimiento adicional, puedes usar [Snoopdigg](https://github.com/botherder/snoopdigg). Snoopdigg es una utilidad que simplifica el proceso de recopilación de información sobre el sistema y toma una instantánea completa de la memoria.
>
> Una herramienta adicional que podría ser útil para recopilar más detalles (pero que requiere cierta familiaridad con los comandos del terminal) es [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) de la Empresa estadounidense de ciberseguridad CrowdStrike.

¿Te ayudaron estas instrucciones a identificar alguna actividad maliciosa?

- [Sí, creo que la computadora está infectada](#device-infected)
- [No, no se identificó ninguna actividad maliciosa](#device-clean)

### device-infected

¡Oh no! Para deshacerse de la infección es posible que desees:

- [Busca ayuda adicional](#malware_end)
- [Procede directamente a un reinicio del dispositivo](#reset).

### reset

> Es posible que desees considerar reiniciar tu dispositivo como una medida de precaución adicional. Las guías a continuación proporcionarán las instrucciones apropiadas para tu tipo de dispositivo:
>
> - [Android](https://support.google.com/nexus/answer/6088915?hl=es)
> - [iOS](https://support.apple.com/es-lamr/HT201252)
> - [Windows](https://support.microsoft.com/es-us/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/es-lamr/HT201314)
> - [Linux (Ubuntu y Mint)](https://www.solvetic.com/tutoriales/article/3686-resetear-de-fabrica-sistema-ubuntu-linux-mint/)

¿Sientes que necesitas ayuda adicional?

- [Sí](#malware_end)
- [No](#resolved_end)

### malware_end

Si necesitas ayuda adicional para tratar un dispositivo infectado, puedes comunicar con las organizaciones que se enumeran a continuación.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Esperamos que el Kit de primeros auxilios digitales DFAK te haya sido útil. Envíanos tus comentarios [por correo electrónico](mailto: incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Estos son algunos consejos para evitar ser víctima de un atacante intentando comprometer tus dispositivos y datos:

- Siempre comprueba la legitimidad de cualquier correo electrónico que recibas, un archivo que hayas descargado o un enlace que solicite los detalles de inicio de sesión de tu cuenta.
- Lee más sobre cómo proteger tu dispositivo contra infecciones de malware en las guías vinculadas en los recursos.

#### Resources

- [Security in a box - Protege tu dispositivo de malware y ataques de phishing](https://securityinabox.org/es/guide/malware/)
- [Security Self-Defense: Cómo evitar los ataques de phishing o suplantación de identidad](https://ssd.eff.org/es/module/c%C3%B3mo-evitar-los-ataques-de-phishing-o-suplantaci%C3%B3n-de-identidad)
- [Security without borders: Guide to Phishing - En Inglés](https://guides.securitywithoutborders.org/guide-to-phishing/)
