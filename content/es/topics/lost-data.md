---
layout: page
title: "Perdí mi información"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: es
summary: "Qué hacer en caso de que pierdas datos"
date: 2019-8
permalink: /es/topics/lost-data
parent: /es/
---

# Perdí mi información

Los datos alojados digitalmente pueden ser muy efímeros e inestables y hay muchas formas de perderlos. El daño físico de tus dispositivos, la cancelación de tus cuentas, el borrado por error, las actualizaciones de software y los errores de software pueden provocar la pérdida de datos. Además, a veces es posible que no sepas si en efecto funciona tu sistema de respaldo, simplemente olvidaste tus credenciales, o la ruta para
encontrar y recuperar tus datos.

Esta sección del kit de primeros auxilios digitales te guiará a través de algunos pasos básicos para diagnosticar cómo podrías haber perdido los datos y las posibles estrategias de mitigación para recuperarlos.

A continuación, un cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### entry-point

> En esta sección nos centramos principalmente en datos basados en dispositivos. Para contenidos y credenciales en línea, te enviaremos a otras secciones del Kit de primeros auxilios digitales.
>
> Estos dispositivos incluyen computadoras, dispositivos móviles, discos duros externos, memorias USB y tarjetas SD.

¿Qué tipo de datos perdiste?

- [Contenido en línea](../../../account-access-issues)
- [Credenciales](../../../account-access-issues)
- [Contenido en dispositivos](#content-on-device)

### content-on-device

> A menudo, los datos "perdidos" representan información que simplemente se ha eliminado, ya sea accidental o intencionalmente. En tu computadora, revisa tu Papelera de reciclaje. En dispositivos Android, puedes encontrar los datos perdidos en el directorio LOST.DIR. Si los archivos perdidos no se pueden ubicar en la Papelera de reciclaje de tu sistema operativo, es posible que no se hayan eliminado, pero que estén en una ubicación diferente a la que esperabas. Prueba la función de búsqueda en tu sistema operativo para localizarlos.
>
> Revisa los archivos y carpetas ocultos, ya que los datos que has perdido pueden estar allí. Así es como puedes hacerlo en [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) y [Windows](https://support.microsoft.com/es-es/windows/ver-carpetas-y-archivos-ocultos-en-windows-10-97fbc472-c603-9d90-91d0-1166d1d9f4b5). Para Linux, ve a tu directorio de inicio en el administrador de archivos y escribe Ctrl + H. Puede ver los archivos ocultos en [Android](https://www.technologyhint.com/show-hidden-files/) e iOS.
>
> Intenta buscar el nombre exacto de tu archivo perdido. Si esto no funciona, o si no estás seguro del nombre exacto, puedes intentar una búsqueda con comodines (es decir, si perdiste un archivo docx, pero no estás seguro del nombre, puedes buscar `*.docx`) que solo mostrará archivos con la extensión `.docx`. Ordena por *Fecha de modificación* para localizar rápidamente los archivos más recientes al usar la opción de búsqueda con comodín.
>
> Además de buscar los datos perdidos en tu dispositivo, pregúntate si has enviado un correo electrónico o lo has compartido con alguien (tu mismo incluido) o lo has agregado a tu almacenamiento en la nube en cualquier momento. Si ese es el caso, puedes realizar una búsqueda allí y es posible que puedas recuperar alguna versión del mismo.
>
> Para aumentar las posibilidades de recuperar tus datos, deja de editar y agregar información a tus dispositivos inmediatamente. Si puedes, conecta la unidad como dispositivo externo (por ejemplo, a través de USB) a una computadora segura separada para buscar y restaurar información. Escribir datos en la unidad (por ejemplo, descargar o crear nuevos documentos) puede disminuir las posibilidades de encontrar y restaurar datos perdidos.

¿Cómo perdiste tus datos?

- [El dispositivo en donde se almacenaron los datos sufrió daño físico](#tech-assistance_end)
- [El dispositivo en donde se almacenaron los datos fue robado/perdido](#device-lost-theft_end)
- [Los datos fueron eliminados](#where-is-data)
- [Los datos desaparecieron después de una actualización de software](#software-update)
- [El sistema o un programa se bloqueó y los datos desaparecieron](#where-is-data)


### software-update

> Algunas veces, cuando actualizas un programa o el sistema operativo completo, pueden ocurrir problemas inesperados, lo que hace que el sistema o el programa deje de funcionar como debería, o que ocurra alguna falla, incluyendo la corrupción de datos. Si notaste una pérdida de datos justo después de una actualización de software o del sistema operativo, vale la pena considerar restaurarlo a un estado anterior. Las restauraciones son útiles porque significan que tu software o bases de datos puede ser revertidas a un estado limpio y consistente, incluso después de que hayan ocurrido operaciones erróneas o bloqueos de software.
>
> El método para restaurar una pieza de software a una versión anterior depende de cómo está construido ese software: en algunos casos es posible de forma sencilla, en otros no, y en algunos se necesita algo de trabajo. Por lo tanto, tendrás que hacer una búsqueda en línea para volver a versiones anteriores. Ten en cuenta que restaurar también se conoce como "desactualizar" (downgrading en Inglés), así que puedes hacer una búsqueda con este término también. Para sistemas operativos como Windows o Mac, cada uno tiene sus propios métodos para restaurar a versiones anteriores.
>
> - Para sistemas Mac, visita la [base de conocimientos del Centro de soporte de Apple](https://support.apple.com/es-lamr) y usa los terminos "volver a" con tu versión macOS para leer más y encontrar instrucciones.
> - Para sistemas Windows, el procedimiento varía dependiendo de si deseas deshacer una actualización específica o restaurar todo el sistema. En ambos casos, puede encontrar instrucciones en el [Centro de soporte de Microsoft](https://support.microsoft.com/es-us); por ejemplo, para Windows 10 puedes buscar la pregunta "¿Cómo elimino una actualización instalada?" En esta página de [preguntas frecuentes de Windows Update](https://support.microsoft.com/es-us/help/12373/windows-update-faq).

¿Fue útil la restauración de software?

- [Sí](#resolved_end)
- [No](#where-is-data)


### where-is-data

> La copia de seguridad de los datos hecha de forma regular es una práctica recomendada. En ocasiones, olvidamos que tenemos la copia de seguridad automática habilitada, por lo que vale la pena revisar si tu dispositivo la tenía, y usar esta copia de seguridad para restaurar tus datos. En caso de que no, te recomendamos la planificación de futuras copias de seguridad, en las [sugerencias finales de este flujo de trabajo](#resolved_end).

Para verificar si tienes una copia de seguridad de los datos que perdiste, comienza por preguntarte en dónde estaban almacenados los datos perdidos.

- [En dispositivos de almacenamiento (disco duro externo, memorias USB, tarjetas SD)](#storage-devices_end)
- [En una computadora](#computer)
- [En un dispositivo móvil](#mobile)

### computer

¿Qué tipo de sistema operativo se ejecuta en tu computadora?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Para verificar si tu dispositivo MacOS tenía la opción de copia de seguridad habilitada y usarla para restaurar tus datos, verifica tu [iCloud](https://support.apple.com/es-lamr/HT208682) o [Time Machine](https://support.apple.com/es-lamr/HT201250)
>
> Un lugar para buscar es la lista de elementos recientes, la cual realiza un seguimiento de las aplicaciones, archivos y servidores que has utilizado durante tus últimas sesiones en la computadora. Para buscar el archivo y volver a abrirlo, ve al Menú de Apple en la esquina superior izquierda, selecciona Elementos recientes y explora la lista de archivos. Se pueden encontrar más detalles en el [artículo del New York Times "Consejo técnico: busca archivos perdidos recientemente en tu Mac" - En Inglés](https://www.nytimes.com/2018/08/01/technology/personaltech/mac-find-archivos-perdidos.html).

¿Pudiste localizar tus datos o restaurarlos?

- [Sí](#resolved_end)
- [No](#macos-restore)

### macos-restore

> En algunos casos, existen herramientas gratuitas y de código abierto que pueden resultar útiles para encontrar el contenido que falta y recuperarlo. Algunas veces, su uso es limitado y la mayoría de las herramientas conocidas cuestan dinero.
>
>Por ejemplo, puedes tratar con el [Asistente de recuperación de datos de EaseUS](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) o [Software de recuperación de datos CleverFiles Disk Drill](https://www.cleverfiles.com/es/).

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech-assistance_end)

### windows-computer

> Para comprobar si tienes una copia de seguridad habilitada en tu máquina Windows, consulta la [Información de soporte de Microsoft: Copia de seguridad y restauración en Windows 10](https://support.microsoft.com/es-es/windows/copia-de-seguridad-y-restauraci%C3%B3n-en-windows-10-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
> Windows 10 incluye la función **Timeline**, que está destinada para mejorar tu productividad al guardar un registro de los archivos que utilizaste, los sitios que navegaste y otras acciones que realizaste en tu computadora. Si no recuerdas dónde almacenaste un documento, puedes hacer clic en el ícono de línea de tiempo en la barra de tareas de Windows 10 para hacer un registro visual organizado por fecha y volver a lo que necesitas, haciendo clic en el ícono de vista previa correspondiente. Esto puede ayudarte a localizar un archivo cuyo nombre ha cambiado. Si la línea de tiempo está habilitada, parte de la actividad de tu PC, como los archivos que editas en Microsoft Office, también se puede sincronizar con tú dispositivo móvil u otra computadora que uses, por lo que es posible que tengas una copia de seguridad de tus datos perdidos en otro dispositivo. Lee más sobre Timeline y cómo usarlo en el [artículo de The New York Times "Consejo técnico: Cómo usar Timeline con Windows 10"](https://www.nytimes.com/2018/07/05/technology/personaltech/windows-10-timeline.html).

¿Pudiste localizar tus datos o restaurarlos?

- [Sí](#resolved_end)
- [No](#windows-restore)

### windows-restore

> En algunos casos, existen herramientas gratuitas y de código abierto que pueden ser útiles para encontrar contenido faltante y recuperarlo. A veces, tienen un uso limitado y la mayoría de las herramientas conocidas cuestan algo de dinero.
>
> Por ejemplo, puedes probar [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_ES) (consulta las [instrucciones paso a paso](https://www.cgsecurity.org/wiki/PhotoRec_Paso_A_Paso), [Asistente de recuperación de datos de EaseUS](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.html), [Software de recuperación de datos CleverFiles Disk Drill](https://www.cleverfiles.com/es/data-recovery-software.html) o [Recuva\](https://www.ccleaner.com/es-es/recuva).

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech-assistance_end)


### linux-computer

> Algunas de las distribuciones de Linux más populares, como Ubuntu, tienen una herramienta de copia de seguridad integrada, por ejemplo [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) en Ubuntu. Si se incluye una herramienta de respaldo integrada en tu sistema operativo, es posible que se te haya solicitado que habilites los respaldos automáticos cuando comenzaste a usar la computadora por primera vez. Busca en tu distribución de Linux para ver si esta incluye una herramienta de copia de seguridad, y cuál es el procedimiento para verificar si está habilitada y restaurar dichos datos.
>
> Lee [Ubuntu & Deja Dup - Levántate, Respalda](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) para comprobar si tienes las copias de seguridad automáticas habilitadas en tú computadora. Lee ["Restauración completa del sistema con Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full) para obtener instrucciones sobre cómo restaurar tus datos perdidos desde una copia de seguridad existente.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech-assistance_end)

### mobile

¿Qué sistema operativo se ejecuta en tu móvil?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Es posible que hayas habilitado una copia de seguridad automática con iCloud o iTunes. Lee ["Restaurar o configurar tu dispositivo desde una copia de seguridad de iCloud"](https://support.apple.com/es-es/guide/icloud/mm7e756df7fd/icloud) para comprobar si tienes copias de seguridad existentes y aprender a restaurar tus datos.

¿Has encontrado tu copia de seguridad y has recuperado tus datos?

- [Sí](#resolved_end)
- [No](#phone-which-data)

###  phone-which-data

¿Cuál de las siguientes opciones se aplica a los datos que perdiste?

- [Son datos generados por aplicaciones, por ejemplo contactos, actualizaciones de contenidos, etc.](#app-data-phone)
- [Son datos generado por el usuario, por ejemplo Fotos, videos, audio, notas](#ugd-phone)

### app-data-phone

> Una aplicación móvil es una aplicación de software diseñada para ejecutarse en tu dispositivo móvil. Sin embargo, a la mayoría de estas aplicaciones también se puede acceder a través de un navegador de escritorio. Si has experimentado una pérdida de datos generados por la aplicación en tu teléfono móvil, intenta acceder a la aplicación desde tu navegador de escritorio, iniciando sesión en la interfaz web de la aplicación con tus credenciales para esa aplicación. Es posible que encuentres los datos que perdiste en la interfaz del navegador.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech-assistance_end)

### ugd-phone

> Los datos generados por el usuario son el tipo de datos que creas o generas a través de una aplicación específica, y en caso de pérdida de datos, querrás verificar si esa aplicación tiene alguna configuración de respaldo habilitada de manera predeterminada o tiene alguna forma de recuperarla. Por ejemplo, si usas WhatsApp en tu móvil y faltan las conversaciones o si ocurre algo indeseado, puedes recuperar tus conversaciones si habilitaste la configuración de recuperación de WhatsApp, o si estás usando una aplicación para crear y mantener notas con información confidencial o personal, también puedes tener una opción de copia de seguridad habilitada sin que lo sepas.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#tech-assistance_end)

### android-phone

> Google tiene un servicio incorporado en Android, llamado Android Backup Service. De forma predeterminada, este servicio realiza copias de seguridad de varios tipos de datos y los asocia con el servicio de Google adecuado, los cuales también puedes acceder a través de la web. Puedes ver tu configuración de sincronización dirigiéndote a Configuración > Cuentas > Google, luego seleccionando tu dirección de Gmail. Si perdiste datos de algún tipo que estabas sincronizando con tu cuenta de Google, probablemente podrás recuperarlos ingresando a tu cuenta a través de un navegador web.

¿Fue útil esta información para recuperar tus datos (total o parcialmente)?

- [Sí](#resolved_end)
- [No](#phone-which-data)


### tech-assistance_end

> Si la pérdida de datos ha sido causada por daños físicos, como caidas al suelo o al agua, eaxposición a cortes de energía eléctrica u otros problemas, la situación más probable es que debas realizar una recuperación de los datos almacenados en el hardware. Si no sabes cómo realizar estas operaciones, debes comunicarse con una persona de TI con equipos electrónicos y hardware de mantenimiento que pueda ayudarte. Sin embargo, dependiendo de su contexto y la sensibilidad de los datos que necesita recuperar, no se recomienda ponerse en contacto con ningún departamento de TI, sino recurrir a personas conocedoras de TI que conoces y en las que confías.


### device-lost-theft_end

> En caso de dispositivos perdidos o robados, asegúrate de cambiar todas tus contraseñas lo antes posible y visitar nuestro [recurso específico sobre qué hacer si se pierde un dispositivo](../../lost-device).
>
> Si eres miembro de la sociedad civil y necesitas asistencia para adquirir un nuevo dispositivo para reemplazar el perdido, puedes comunicarse con las organizaciones que se enumeran a continuación.

:[](organisations?services=equipment_replacement)


### storage-devices_end

> Para recuperar los datos perdidos, recuerda que el tiempo es importante. Por ejemplo, recuperar un archivo que eliminaste accidentalmente unas pocas horas o un día antes podría tener mayores tasas de éxito que un archivo que perdiste meses antes.
>
> Considera utilizar una herramienta de software para intentar recuperar los datos que has perdido (debido a la eliminación o corrupción) como [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec_ES) (para Windows, Mac o Linux: consulta [instrucciones paso a paso](https://www.cgsecurity.org/wiki/PhotoRec_Paso_A_Paso),[Asistente de recuperación de datos de EaseUS](https://www.easeus.com/data-recovery-software/) (para Windows, Mac, Android o iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/es/) (para Windows o Mac) o [Recuva](https://www.ccleaner.com/es-es/recuva) (para Windows). Ten en cuenta que estas herramientas no siempre funcionan, porque tu sistema operativo pudo haber escrito nuevos datos sobre la información eliminada. Debido a esto, debes hacer lo menos posible con tu computadora después de eliminar un archivo e intentar restaurarlo con una de las herramientas anteriores.
>
> Para aumentar tus posibilidades de recuperar datos, deja de usar tus dispositivos inmediatamente. La escritura continua en el disco duro puede disminuir las posibilidades de encontrar y restaurar datos.
>
> Si ninguna de las opciones anteriores te ha funcionado, la situación más probable es que debas realizar una recuperación de los datos almacenados en el disco duro. Si no sabes cómo realizar estas operaciones, debe comunicarse con una persona de TI con equipos
electrónicos y hardware de mantenimiento que pueda ayudarte. Sin embargo, dependiendo del caso y la sensibilidad de los datos que necesitas recuperar, no se recomienda ponerse en contacto con cualquier departamento de TI, sino recurrir de preferencia a expertos en TI que conoces y en las que confías.

### resolved_end

Esperamos que esta guía de DFAK haya sido útil. Por favor danos tu opinión [vía email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Copias de seguridad o respaldos - Siempre es buena idea asegurarse de tener copias de seguridad o respaldos- ¡varias copias de seguridad diferentes, almacenadas en otro lugar que no sea el mismo lugar donde están tus datos originales! Dependiendo de tu caso, opta por almacenar esas copias de seguridad en servicios tales como una nube y en dispositivos físicos externos que mantengas desconectado de tú computadora mientras se conecta a Internet.
- Para ambos tipos de copias de seguridad, debes proteger tus datos con cifrado. Realiza copias de seguridad periódicas e incrementales de tus datos más importantes, verifica que los tengas listos y prueba que puedes restaurarlos antes de realizar actualizaciones de software o sistemas operativos.
- Configura un sistema de carpetas estructurado - Cada persona tiene su propia forma de organizar sus datos e información importantes, no hay una solución única para todos. No obstante, es importante que consideres configurar un sistema de carpetas que se adapte a tus necesidades. Al crear un sistema de carpetas consistente, puedes hacer tu vida más fácil al saber mejor cuáles carpetas y archivos se deben respaldar con frecuencia, por ejemplo, en donde se encuentre la información importante en la que estás trabajando, en donde debes mantener los datos que contienen información personal o confidencial, tus colaboradores, y así sucesivamente. Como de costumbre, respira hondo y toma un tiempo para planificar el tipo de datos que produces o administras, y piensa en un sistema de carpetas que puedas hacer más coherente y ordenado.

#### Resources

- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Respaldo seguro - En Inglés](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a box: Recuperar información perdida](https://securityinabox.org/es/guide/backup/)
- [Página oficial sobre copias de seguridad para Mac](https://support.apple.com/es-lamr/mac-backup)
- [Copias de seguridad y restauración en Windows 10](https://support.microsoft.com/es-us/help/4027408/windows-10-backup-and-restore)
- [Cómo habilitar copias de seguridad regulares en el iPhone](https://support.apple.com/es-lamr/HT203977)
- [Cómo habilitar respaldos de seguridad programados en Android](https://support.google.com/android/answer/2819582?hl=es)
- [Respaldo de seguridad de datos de forma segura](https://johnopdenakker.com/securily-backup-your-data/)
- [Cómo hacer un respaldo de seguridad de tu vida digital](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Recuperación de Datos](https://es.wikipedia.org/wiki/Recuperación_de_datos)
