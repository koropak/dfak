---
layout: page.pug
title: "A propos"
language: fr
summary: "A propos du kit de premiers soins numériques."
date: 2021-01-27
permalink: /fr/about/
parent: Home
---

La trousse de premiers soins numériques est le fruit d'une collaboration entre le [RaReNet (Rapid Response Network)](https://www.rarenet.org/) et le [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Le Réseau de réponse rapide est un réseau international d'intervenants réactifs et d'experts en sécurité numérique qui comprend Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtual Road, ainsi que des experts individuels en sécurité qui travaillent dans le domaine de la sécurité numérique et des interventions d'urgence.

Certaines de ces organisations et personnes font partie de CiviCERT, un réseau international de services d'assistance en matière de sécurité numérique et de fournisseurs d'infrastructures qui se concentrent principalement sur le soutien aux groupes et organisations luttant pour la justice sociale et la défense des droits humains et numériques. CiviCERT est un cadre professionnel pour soutenir les efforts distribués de la communauté d'intervention rapide CERT (Computer Emergency Response Team). CiviCERT est accrédité par Trusted Introducer, le réseau européen d'équipes de confiance pour l'intervention d'urgence en informatique.

La trousse de premiers soins numérique est également un projet [open-source qui accepte des contributions extérieures](https://gitlab.com/rarenet/dfak).

Si vous souhaitez utiliser la trousse de premiers soins numériques alors que la connectivité est limitée, ou trouver une connexion est difficile, vous pouvez télécharger une version [ici](https://digitalfirstaid.org/dfak-offline.zip).

Pour tout commentaire, suggestion ou question à propos de la trousse de premiers soins numériques, vous pouvez nous écrire à: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
