---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 hours
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

En complément de son activité principale, Internews travaille également avec des individus, des organisations et des communautés dans le monde pour améliorer la compréhension des problèmes de sécurité numérique, protéger l’accès à un internet libre et non censuré, et améliorer les pratiques de sécurité numérique. Internews a formé des journalistes et des défenseurs des droits de l’homme dans plus de 80 pays et dispose d’un réseau solide de formateurs locaux et régionaux en sécurité numérique ainsi que des professionnels formé au Security Auditing Framework and Evaluation Template for Advocacy Groups (SAFETAG) framework (https://safetag.org), dont le développement a été supervisé par Internews. Internews établit des partenariats solides et réactifs avec des sociétés de renseignement et d'analyse des menaces, à la fois issues de la société civile et du secteur privé, et peut aider directement ses partenaires à maintenir une présence en ligne sûre et non censurée. Internews propose des interventions techniques et non techniques allant des évaluations de sécurité de base utilisant le cadre SAFETAG aux évaluations des politiques organisationnelles et aux stratégies d'atténuation basées sur la recherche de menaces. Ils soutiennent directement l'analyse du phishing (hameçonnage) et des logiciels malveillants pour les groupes de défense des droits humains et des médias qui sont victimes d'attaques numériques ciblées.
