---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: de 8h à 18h du lundi au dimanche CET
response_time: 4 heures
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation est un fournisseur de solutions de sécurité pour les médias indépendants, les organisations de défense des droits humains, les journalistes d'investigation et les militants. Qurium fournit un portefeuille de solutions professionnelles, individualisées et sécurisées avec un soutien personnel aux organisations et aux individus à risque, qui comprend:

- Hébergement sécurisé avec réduction des attaques DDoS des sites web à risque
- Réponse rapide en soutien aux organisations et aux personnes immédiatement menacées
- Audits de sécurité des services web et des applications mobiles
- Contournement des sites internet bloqués
- Investigations techniques sur les attaques numériques, les applications frauduleuses, les logiciels malveillants ciblés et la désinformation
