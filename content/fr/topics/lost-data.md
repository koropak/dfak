---
layout: page
title: "J'ai perdu mes données"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: fr
summary: "Que faire en cas de perte de données ?"
date: 2021-01-28
permalink: /fr/topics/lost-data
parent: /fr/
---

# I Lost my Data

Les données numériques peuvent être très éphémères et instables et il existe de nombreuses façons de les perdre. L'endommagement physique de vos appareils, la résiliation de vos comptes, la suppression par erreur, les mises à jour de logiciels et les plantages de logiciels peuvent tous être la cause d'une perte de données. En outre, il arrive parfois que vous ne sachiez pas comment ou si votre système de sauvegarde fonctionne, ou que vous ayez simplement oublié vos accès ou la démarche à suivre pour trouver ou récupérer vos données.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour diagnostiquer comment vous pourriez avoir perdu des données et vous donnera des stratégies d'atténuation potentielles pour les récupérer.

Voici un questionnaire pour identifier la nature de votre problème et trouver des solutions possibles.


## Workflow

### entry-point

> Dans cette section, nous nous concentrons principalement sur les données stockées dans des appareils. En ce qui concerne le contenu en ligne et les identifiants, nous vous dirigerons vers d'autres sections de la trousse de premiers soins numériques.
>
> Les appareils en question comprennent les ordinateurs, les appareils mobiles, les disques durs externes, les clés USB et les cartes SD.

Quel type de données avez-vous perdu ?

- [Contenu en ligne](../../../account-access-issues)
- [Identifiant de compte](../../../account-access-issues)
- [Contenu sur un appareil](#content-on-device)

### content-on-device

> Souvent, vos données "perdues" sont des données qui ont simplement été effacées - accidentellement ou intentionnellement. Dans votre ordinateur, vérifiez votre Corbeille. Sur les appareils Android, vous pouvez trouver les données perdues dans le répertoire LOST.DIR. Si les fichiers manquants ou perdus ne peuvent pas être localisés dans la Corbeille de votre système d'exploitation, il est possible qu'ils n'aient pas été supprimés, mais qu'ils se trouvent à un emplacement différent de celui que vous aviez prévu. Essayez la fonction de recherche dans votre système d'exploitation pour les localiser.
>
> Vérifiez également les fichiers et dossiers cachés, car les données que vous avez perdues peuvent s'y trouver. Voici comment procéder sur [Mac](https://www.papergeek.fr/comment-afficher-les-fichiers-caches-sur-mac-132835) et [Windows](https://support.microsoft.com/fr-fr/windows/afficher-les-fichiers-cach%C3%A9s-0320fe58-0117-fd59-6851-9b7f9840fdb2). Pour Linux, allez dans votre répertoire personnel dans le gestionnaire de fichiers et tapez Ctrl + H. Vous pouvez également consulter les fichiers cachés sur [Android](https://fr.androiddata-recovery.com/blog/comment-afficher-et-recuperer-des-fichiers-caches-sur-android) et iOS.
>
> Essayez de rechercher le nom exact de votre fichier perdu. Si cela ne fonctionne pas, ou si vous n'êtes pas sûr du nom exact, vous pouvez essayer une recherche de caractères génériques (par exemple, si vous avez perdu un fichier docx, mais que vous n'êtes pas sûr du nom, vous pouvez rechercher `*.docx`) qui ne fera apparaître que les fichiers avec l'extension `.docx`. Trier par *Date Modifié* afin de localiser rapidement les fichiers les plus récents lors de l'utilisation de l'option de recherche avec un caractère générique `*` (wildcard).
>
> En plus de rechercher les données perdues sur votre appareil, demandez-vous si vous les avez envoyées par e-mail ou partagées avec quelqu'un (y compris vous-même) ou si vous les avez ajoutées à votre stockage dans le cloud à un moment donné. Si c'est le cas, vous pouvez y effectuer une recherche et peut être, être en mesure de récupérer les données ou au moins une version de celles-ci.
>
> Pour augmenter vos chances de récupérer des données, arrêtez immédiatement de modifier et d'ajouter des informations sur vos appareils. Si vous le pouvez, connectez le lecteur comme périphérique externe (par exemple via USB) à un ordinateur séparé et sécurisé pour rechercher et restaurer des informations. L'écriture sur le disque (par exemple, le téléchargement ou la création de nouveaux documents) peut réduire les chances de retrouver et de restaurer les données perdues.

Comment avez-vous perdu vos données ?

- [L'appareil où les données ont été stockées a subi un dommage physique](#tech-assistance_end)
- [Le périphérique où les données ont été stockées a été volé/perdu](#device-lost-theft_end)
- [Les données ont été supprimées](#where-is-data)
- [Les données ont disparu après une mise à jour du logiciel](#software-update)
- [Le système ou un outil logiciel a planté et les données ont disparu](#where-is-data)


### software-update

> Parfois, lorsque vous mettez à jour ou mettez à niveau un outil logiciel ou l'ensemble du système d'exploitation, des problèmes inattendus peuvent survenir, entraînant l'arrêt du fonctionnement du système ou du logiciel comme il se doit, ou un comportement défectueux, ce qui peut entrainer la corruption des données. Si vous avez remarqué une perte de données juste après une mise à jour du logiciel ou du système, il vaut la peine d'envisager de revenir à un état antérieur. Les reculs sont utiles parce qu'ils signifient que votre logiciel ou base de données peut être restauré à un état propre et cohérent même après des opérations erronées ou lorsque des pannes logicielles se sont produites.
>
> La méthode pour ramener un logiciel à une version précédente dépend de la façon dont il a été installé - dans certains cas, c'est possible facilement, dans d'autres cas, cela impliquera un travail plus ou moins conséquent. Vous devrez donc faire une recherche en ligne sur le retour aux anciennes versions du logiciel en question. Veuillez noter que le retour en arrière est également connu sous le nom de "downgrading", alors faites une recherche avec ce terme aussi. Pour les systèmes d'exploitation tels que Windows ou Mac, chacun a ses propres méthodes pour revenir à la version précédente.
>
> * Pour les systèmes Mac, visitez la base de connaissances [Mac Support Center](https://support.apple.com/fr-fr) et utilisez le mot-clé "downgrade" avec votre version MacOS pour en savoir plus et trouver les instructions.
> * Pour les systèmes Windows, la procédure varie selon que vous souhaitez annuler une mise à jour spécifique ou revenir en arrière sur l'ensemble du système. Dans les deux cas, vous trouverez les instructions dans le [Microsoft Support Center](https://support.microsoft.com/fr-fr), par exemple pour Windows 10 vous pouvez chercher la question "Comment supprimer une mise à jour installée" dans cette [page FAQ](https://support.microsoft.com/fr-fr/help/12373/windows-update-faq).

Le retour à une version précédente du logiciel a-t-elle été utile ?

- [Oui](#resolved_end)
- [Non](#where-is-data)


### where-is-data

> La sauvegarde régulière des données est une bonne pratique à recommander. Parfois, nous oublions que la sauvegarde automatique est activée, il vaut donc la peine de vérifier sur vos appareils si cette option est activée, et d'utiliser cette sauvegarde pour restaurer vos données. Si ce n'est pas le cas, il est recommandé de planifier les sauvegardes futures, et vous trouverez d'autres conseils sur la façon de configurer les sauvegardes dans les [derniers conseils de ce déroulé](#resolved_end).

Pour vérifier si vous avez une sauvegarde des données perdues, commencez à vous demander où les données perdues ont été stockées.

- [Dans un périphérique de stockage (disque dur externe, clés USB, cartes SD)](#storage-devices_end)
- [Dans un ordinateur](#computer)
- [Dans un appareil mobile](#mobile)


### computer

Quel type de système d'exploitation est installé sur votre ordinateur ?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Pour vérifier si une option de sauvegarde est activée sur votre périphérique MacOS et utiliser ainsi cette sauvegarde pour restaurer vos données, vérifiez vos options [iCloud](https://support.apple.com/fr-fr/HT208682) ou [Time Machine](https://support.apple.com/fr-fr/HT201250) pour voir si une sauvegarde est disponible.
>
> Vous pouvez consulter la liste des éléments récents, qui répertorie les applications, les fichiers et les serveurs que vous avez utilisés au cours de vos dernières sessions sur l'ordinateur. Pour rechercher le fichier et le rouvrir, allez dans le menu Apple dans le coin supérieur gauche, sélectionnez Éléments récents et parcourez la liste des fichiers. Vous trouverez plus de détails dans l'article "Trouvez les fichiers récemment perdus sur votre Mac"](https://fr.wikihow.com/r%C3%A9cup%C3%A9rer-des-fichiers-accidentellement-supprim%C3%A9s-sous-Mac-OS-X).

Avez-vous pu localiser vos données ou les restaurer ?

- [Oui](#resolved_end)
- [Non](#macos_restore)

### macos-restore

> Dans certains cas, il existe des outils libres qui peuvent être utiles pour trouver le contenu manquant et le récupérer. Leur utilisation est parfois limitée, et la plupart des outils connus coûtent un peu d'argent.
>
> Par exemple vous pouvez essayer [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) ou [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/).

Cette information vous a t-elle été utile (complètement ou partiellement)?

- [Oui](#resolved_end)
- [Non](#tech-assistance_end)

### windows-computer

> Pour vérifier si une sauvegarde automatique est activée sur votre machine Windows, voir [Microsoft Support technique: Sauvegarder et restaurer votre PC](https://support.microsoft.com/fr-fr/windows/sauvegarder-et-restaurer-votre-pc-ac359b36-7015-4694-de9a-c5eac1ce9d9c).
>
> Windows 10 inclut la fonction **Timeline**, qui vise à améliorer votre productivité en enregistrant les fichiers que vous avez utilisés, les sites que vous avez visités et les autres actions que vous avez effectuées sur votre ordinateur. Si vous ne vous souvenez plus de l'endroit où vous avez stocké un document, vous pouvez cliquer sur l'icône "Chronologie" dans la barre des tâches de Windows 10 pour voir un journal visuel organisé par date, et revenir à ce dont vous avez besoin en cliquant sur l'icône d'aperçu appropriée. Cela peut vous aider à localiser un fichier qui a été renommé. Si l'option Timeline est activée, certaines de vos activités sur PC - comme les fichiers que vous modifiez dans Microsoft Office - peuvent également se synchroniser avec votre appareil mobile ou un autre ordinateur que vous utilisez, de sorte que vous pourriez avoir une sauvegarde de vos données perdues dans un autre appareil. Pour en savoir plus sur Timeline et son utilisation, consultez l'article  ["Utilisation de la chronologie dans Windows 10"](https://support.microsoft.com/fr-fr/windows/obtenir-de-l-aide-pour-la-chronologie-febc28db-034c-d2b0-3bbe-79aa0c501039).

Avez-vous pu localiser vos données ou les restaurer ?

- [Oui](#resolved_end)
- [Non](#windows-restore)


### windows-restore

> Dans certains cas, il existe des outils gratuits et open-source qui peuvent être utiles pour trouver le contenu manquant et le récupérer. Parfois, leur utilisation est limitée et la plupart des outils connus coûtent de l'argent.
>
> Par exemple, vous pouvez essayer [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (voir [les instructions étape par étape](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/data-recovery-software.html), ou [Recuva](http://www.ccleaner.com/recuva).

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#tech-assistance_end)


### linux-computer

> Certaines des distributions Linux les plus populaires, comme Ubuntu, ont un outil de sauvegarde intégré, par exemple [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) sur Ubuntu. Si un outil de sauvegarde intégré est inclus dans votre système d'exploitation, vous avez peut-être été invité à activer les sauvegardes automatiques lorsque vous avez commencé à utiliser votre ordinateur. Effectuez une recherche dans votre distribution Linux pour voir si elle inclut un outil de sauvegarde intégré, et quelle est la procédure à suivre pour vérifier si il est activé et pour restaurer les données à partir des sauvegardes.
>
> A Lire [Ubuntu & Deja Dup pour la sauvegarde](https://doc.ubuntu-fr.org/deja-dup) notamment pour voir si la sauvegarde automatique est déjà activée sur votre ordinateur. A lire également ["Réinitialisation à l’identique du système"](https://doc.ubuntu-fr.org/reinstallation_a_l_identique) pour des instructions sur comment restaurer vos données et votre système depuis une sauvegarde existante.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#tech-assistance_end)


### mobile

Quel système d'exploitation fonctionne sur votre mobile ?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Vous avez peut-être activé une sauvegarde automatique avec iCloud ou iTunes. Lire ["Restaurer ou configurer votre appareil à partir d’une sauvegarde iCloud"](https://support.apple.com/fr-fr/guide/icloud/mm7e756df7fd/icloud) pour voir si vous avez des sauvegardes existantes et apprendre comment restaurer vos données.

Avez-vous trouvé votre sauvegarde et récupéré vos données ?

- [Oui](#resolved_end)
- [Non](#phone-which-data)

###  phone-which-data

Lequel des énoncés suivants s'applique aux données que vous avez perdues ?

- [Il s'agit de données générées par des applications, par exemple les contacts, les flux, etc.](#app-data-phone)
- [Il s'agit de données générées par l'utilisateur, par exemple photos, vidéos, audio, notes](#ugd-phone)

### app-data-phone

> Une application mobile est une application logicielle conçue pour fonctionner sur votre appareil mobile. Cependant, la plupart de ces applications sont également accessibles via un navigateur sur un ordinateur de bureau. Si vous avez subi une perte de données générées par les applications dans vos téléphones mobiles, essayez d'accéder à cette application dans votre navigateur d'ordinateur de bureau, en vous connectant à l'interface Web de l'application avec vos identifiants pour cette application. Il se peut que vous y trouviez les données perdues via l'interface du navigateur.

- [Oui](#resolved_end)
- [Non](#tech-assistance_end)

### ugd-phone

> Les données générées par l'utilisateur sont le type de données que vous créez ou générez via une application spécifique, et en cas de perte de données, vous pouvez vérifier si les paramètres de sauvegarde de cette application sont activés par défaut ou si elle permet de les restaurer. Par exemple, si vous utilisez WhatsApp sur votre mobile et que les conversations ont disparu ou que quelque chose de défectueux s'est produit, vous pouvez récupérer vos conversations si vous avez activé le paramètre de récupération de WhatsApp, ou si vous utilisez une application pour créer et conserver des notes contenant des informations sensibles ou personnelles, elle peut également avoir une option de sauvegarde active sans que vous le remarquiez parfois.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#tech-assistance_end)

### android-phone

> Google a un service intégré dans Android, appelé Android Backup Service. Par défaut, ce service sauvegarde plusieurs types de données et les associe au service Google approprié, auquel vous pouvez également accéder sur le Web. Vous pouvez voir vos paramètres de synchronisation en allant dans Paramètres > Comptes > Google, puis en sélectionnant votre adresse Gmail. Si vous avez perdu des données que vous synchronisiez avec votre compte Google, vous pourrez probablement les récupérer en vous connectant à votre compte Google via un navigateur Web.

Ces informations vous ont-elles été utiles pour récupérer vos données (en tout ou en partie) ?

- [Oui](#resolved_end)
- [Non](#phone-which-data)


### tech-assistance_end

> Si votre perte de données a été causée par un dommage physique tel que la chute de vos appareils sur le sol ou dans l'eau, une panne d'électricité ou d'autres problèmes, vous devrez certainement procéder à une récupération des données stockées dans votre matériel. Si vous ne savez pas comment effectuer ces opérations, vous devez contacter un responsable informatique chargé de la maintenance du matériel et des équipements électroniques qui pourrait vous aider. Toutefois, en fonction de votre contexte et de la sensibilité des données que vous devez récupérer, il n'est pas conseillé de contacter un magasin, mais plutôt de contacter des spécialistes informatiques que vous connaissez et en qui vous avez confiance.


### device-lost-theft_end

> En cas de perte ou de vol d'appareils, assurez-vous de changer tous vos mots de passe dès que possible et de visiter notre [ressource spécifique sur ce qu'il faut faire si un appareil est perdu](../../../I lost my devices).
>
> Si vous êtes membre de la société civile et que vous avez besoin d'aide pour acquérir un nouvel appareil afin de remplacer celui que vous avez perdu, vous pouvez vous adresser aux organisations énumérées ci-dessous.

:[](organisations?services=equipment_replacement)


### storage-devices_end

> Pour récupérer les données que vous avez perdues, rappelez-vous que le timing est important. Par exemple, la récupération d'un fichier que vous avez supprimé accidentellement quelques heures ou la veille peut avoir un taux de réussite supérieur à celui d'un fichier perdu des mois auparavant.
>
> Envisagez d'utiliser un outil logiciel pour tenter de récupérer les données que vous avez perdues (en raison de la suppression ou de la corruption) tels que [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (pour Windows, Mac, or Linux - voir [instructions étape par étape](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (pour Windows, Mac, Android, ou iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/) (pour Windows ou Mac), ou [Recuva](http://www.ccleaner.com/recuva) (pour Windows). Sachez que ces outils ne fonctionnent pas toujours, car votre système d'exploitation peut avoir inscrit de nouvelles données sur les informations supprimées. Pour cette raison, vous devez faire le moins possible avec votre ordinateur entre la suppression d'un fichier et la tentative de le restaurer avec un des outils ci-dessus.
>
> Pour augmenter vos chances de récupérer des données, cessez immédiatement d'utiliser vos appareils. L'écriture continue sur le disque dur peut diminuer les chances de trouver et de restaurer les données.
>
> Si aucune des options ci-dessus n'a fonctionné pour vous, vous devrez probablement procéder à une récupération des données stockées sur le disque dur. Si vous ne savez pas comment effectuer ces opérations, vous devez contacter un responsable informatique chargé de la maintenance du matériel et des équipements électroniques qui pourrait vous aider. Toutefois, en fonction de votre contexte et de la sensibilité des données que vous devez récupérer, il n'est pas conseillé de contacter un magasin, mais plutôt de contacter des spécialistes informatiques que vous connaissez et en qui vous avez confiance.


### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Sauvegardes - C'est toujours une bonne idée de s'assurer que vous avez des sauvegardes - beaucoup de sauvegardes différentes, à un autre endroit que vos données! En fonction de votre contexte, optez pour le stockage de vos sauvegardes dans des services type cloud et dans des dispositifs physiques externes que vous maintenez déconnectés de votre ordinateur lorsque vous vous connectez à Internet.
- Pour les deux types de sauvegardes, vous devez protéger vos données par un chiffrement. Effectuez des sauvegardes régulières et incrémentielles de vos données les plus importantes, vérifiez qu'elles sont prêtes et testez que vous êtes en mesure de les restaurer avant d'effectuer des mises à jour de logiciels ou de systèmes d'exploitation.
- Mettez en place un système de dossiers structurés - Chaque personne a sa propre façon d'organiser ses données et informations importantes, il n'y a pas de règle unique pour tous. Néanmoins, il est important que vous envisagiez de mettre en place un système de dossiers adapté à vos besoins. En créant un système de dossiers cohérent, vous pouvez vous faciliter la vie en sachant mieux quels dossiers et fichiers doivent être sauvegardés fréquemment, par exemple, où se trouvent les informations importantes sur lesquelles vous travaillez, où vous devez conserver les données contenant des informations personnelles ou sensibles sur vous et vos collaborateurs, etc. Prenez le temps pour planifier le type de données que vous produisez ou gérez, et pensez à un système de dossiers qui peut rendre votre organisation plus cohérente.


#### resources

- [La documentation publique d’assistance à la sécurité numérique d’Access Now : Des sauvegardes sûres](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a Box: Tactiques de sauvegarde](https://securityinabox.org/en/guide/backup/)
- [Page officielle sur les sauvegardes Mac](https://support.apple.com/fr-fr/mac-backup)
- [Comment sauvegarder régulièrement des données sous Windows 10](https://support.microsoft.com/fr-fr/help/4027408/windows-10-backup-and-restore)
- [Comment activer les sauvegardes régulières sur iPhone](https://support.apple.com/fr-fr/HT203977)
- [Activer des sauvegardes régulières sur Android](https://support.google.com/android/answer/2819582?hl=fr)
- [Sauvegarder de manière sécurisée vos données](https://guide.boum.org/tomes/1_hors_connexions/unepage/#index28h2)
- [Sauvegarder votre vie numérique](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Récupération de données](https://fr.wikipedia.org/wiki/R%C3%A9cup%C3%A9ration_de_donn%C3%A9es)
