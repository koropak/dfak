---
layout: page
title: Tor
author: mfc
language: id
summary: Contact methods
date: 2018-09
permalink: /id/contact-methods/tor.md
parent: /id/
published: true
---

The Tor Browser is a privacy-focused web browser that enables you to interact with websites anonymously by not sharing your location (via your IP address) when you access the website.

Resources: [Overview of Tor](https://www.torproject.org/about/overview.html.en).