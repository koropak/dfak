---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: darurat 24jam 7 hari, global; kerja regular Senin-Jumat pada jam kerja, IST (UTC+1), staf berlokasi di berbagai zona waktu di berbagai wilayah.
response_time: hari yang sama atau hari berikutnya untuk kondisi darurat
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders adalah organisasi internasional yang berbasis di Irlandia yang bekerja untuk perlindungan terpadu para pembela hak asasi manusia yang berisiko. Front Line Defenders memberikan dukungan cepat dan praktis kepada para pembela hak asasi manusia yang menghadapi risiko langsung melalui hibah keamanan, pelatihan keamanan fisik dan digital, advokasi dan kampanye.

Front Line Defenders mengoperasikan hotline dukungan darurat 24/7 pada +353-121-00489 untuk pembela hak asasi manusia yang terancam bahaya dalam bahasa Arab, Inggris, Prancis, Rusia atau Spanyol. Ketika pembela hak asasi manusia menghadapi ancaman langsung terkait hidupnya, Front Line Defenders dapat membantu relokasi sementara. Front Line Defenders memberikan pelatihan keamanan fisik dan keamanan digital. Front Line Defenders juga mempublikasikan kasus-kasus pembela hak asasi manusia yang berisiko dan kampanye serta advokasi di tingkat internasional termasuk UE, PBB, mekanisme antar-kawasan dan dengan pemerintah secara langsung untuk perlindungan mereka.
