---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: 8am-18pm, Mon-Sun CET
response_time: 4 jam
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation adalah penyedia solusi keamanan untuk media independen, organisasi hak asasi manusia, jurnalis investigas dan aktivis. Qurium menyediakan solisi aman profesional, yang bisa disesuaikan dengan kebutuhan disertai dukungan personal organisasi dan individu yang berisiko. Layanan melingkupi:
- Hosting aman dengan mitigasi DDoS untuk website yang berisiko
- Dukungan respon cepat untuk organisasi dan individu dalam ancaman langsung
- Audit keamanan untuk website dan aplikasi mobile
- Membuka website yang diblok
- Investigasi forensik atas serangan digital, aplikasi tipuan, malware yang ditarget, dan disinformasi
