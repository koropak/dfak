---
layout: page.pug
language: id
permalink: /id/support
type: support
---

Berikut adalah daftar organisasi yang menyediakan berbagai jenis bantuan. Klik tiap-tiap organisasi untuk melihat info selengkapnya mengenai layanan serta cara menghubungi mereka.
