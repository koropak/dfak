---
layout: page
title: "Seseorang Memalsukan Identitas Saya secara Online"
author: Floriana Pagano, Alexandra Hache
language: id
summary: "Seseorang ditiru identitasnya melalui akun media sosial, alamat email, kunci PGP, situs web atau aplikasi palsu"
date: 2020-11
permalink: /id/topics/impersonated
parent: Home
---

# Seseorang Memalsukan Identitas Saya secara Online

Sebuah ancaman yang dihadapi oleh banyak aktivis, pembela HAM, LSM, media independen, dan blogger adalah pemalsuan identitasnya oleh lawan yang akan menciptakan profil, situs web, atau email palsu atas nama mereka. Hal ini kadang dimaksudkan untuk menciptakan kampanye kotor, informasi yang salah, rekayasa sosial, atau pencurian identitas seseorang untuk menciptakan keributan, masalah kepercayaan, dan pelanggaran data yang mempengaruhi reputasi individu dan kolektif yang ditirukan. Pada kasus lain, lawan tersebut bisa jadi menirukan identitas daring seseorang untuk kepentingan finansial, seperti penggalangan dana, pencurian kredensial pembayaran, menerima pembayaran, dll.

Ini adalah masalah yang membuat frustasi, yang dapat mempengaruhi kapasitas Anda untuk berkomunikasi dan memberi informasi pada berbagai tingkatan. Hal ini juga dapat memiliki penyebab yang berbeda, tergantung di mana dan bagaimana Anda dipalsukan.

Penting untuk diketahui bahwa ada berbagai cara untuk memalsukan identitas seseorang (profil palsu di media sosial, situs web kloningan, email palsu, publikasi gambar dan video pribadi tanpa persetujuan). Strateginya dapat berkisar dari mengirimkan pemberitahuan penghapusan, membuktikan kepemilikan asli, mengklaim hak cipta situs web atau informasi asli, atau memperingatkan jaringan dan kontak pribadi Anda melalui komunikasi publik atau rahasia. Mendiagnosis masalah dan menemukan solusi yang memungkinkan untuk kasus pemalsuan identitas bisa jadi rumit. Terkadang hampir tidak mungkin untuk mendorong perusahaan hosting kecil untuk menghapus sebuah situs web, dan tindakan hukum mungkin diperlukan. Ada baiknya memasang peringatan dan memantau internet untuk mencari tahu apakah Anda atau organisasi Anda sedang dipalsukan identitasnya.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mendiagnosis kemungkinan-kemungkinan cara pemalsuan identitas dan strategi penanggulangan yang memungkinkan untuk menghapus akun, situs web, dan email yang memalsukan identitas Anda atau organisasi Anda.

Jika Anda menjadi korban pemalsuan identitas, ikuti kuesioner ini untuk mengidentifikasi sifat masalah Anda dan mencari solusi yang memungkinkan.


## Workflow

### urgent-question

Apakah Anda mengkhawatirkan kesehatan fisik dan kesejahteraan Anda?

 - [Ya](#physical-sec_end)
 - [Tidak](#diagnostic-start1)

### diagnostic-start1

Apakah pemalsuan identitas ini mempengaruhi Anda secara individu (seseorang menggunakan nama dan nama keluarga resmi Anda, atau nama panggilan yang menjadi dasar reputasi Anda) atau sebagai sebuah organisasi/kolektif?

- [Sebagai individu](#individual)
- [Sebagai organisasi](#organization)

### individual

> Jika Anda terpengaruh secara individu, Anda mungkin ingin memberitahu kontak Anda. Lakukan langkah ini menggunakan akun email, profil, atau situs web yang sepenuhnya ada dalam kendali Anda.

- Begitu Anda telah memberitahu kontak Anda bahwa ada yang meniru identitas Anda, lanjutkan ke [langkah selanjutnya](#diagnostic-start2).

### organization

> Jika Anda terpengaruh secara kelompok, sebaiknya Anda melakukan komunikasi publik. Lakukan langkah ini menggunakan akun email, profil, atau situs web yang sepenuhnya ada dalam kendali Anda.

- Begitu Anda telah memberitahu kontak Anda bahwa ada yang meniru identitas Anda, lanjutkan ke [langkah selanjutnya](#diagnostic-start2).

### diagnostic-start2

Bagaimana bentuk pemalsuan identitas yang Anda alami?

 - [Ada situs web palsu berkedok sebagai saya atau kelompok saya](#fake-website)
 - [Melalui akun jejaring sosial ](#social-network)
 - [Melalui pembagian video atau gambar tanpa persetujuan](#other-website)
 - [Melalui alamat email saya atau alamat yang serupa](#spoofed-email1)
 - [Melalui kunci PGP yang terhubung dengan alamat email saya](#PGP)
 - [Melalui aplikasi palsu yang meniru aplikasi saya](#app1)

### social-network

Pada platform jejaring sosial manakah identitas Anda dipalsukan?

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google+](#google+)
- [Instagram](#instagram)

### facebook

> Ikuti petunjuk di [“Bagaimana cara melaporkan akun atau Halaman Facebook yang berpura-pura sebagai saya atau orang lain?”](https://www.facebook.com/help/174210519303259) untuk meminta akun yang berkedok sebagai Anda itu dihapus.
>
> Harap diingat bahwa mungkin akan butuh beberapa saat untuk menerima jawaban atas permintaan Anda. Simpan halaman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah petunjuk ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### twitter

> Ikuti langkah-langkah di [“Laporkan akun untuk peniruan identitas”](https://help.twitter.com/forms/impersonation) untuk meminta akun yang berkedok sebagai Anda itu dihapus.
>
> Harap diingat bahwa mungkin akan butuh beberapa saat untuk menerima jawaban atas permintaan Anda. Simpan halaman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah petunjuk ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### google+

> Ikuti langkah-langkah di [laman “Laporkan pemalsuan identitas” ](https://support.google.com/plus/troubleshooter/1715140?hl=id) untuk meminta akun yang berkedok sebagai Anda itu dihapus.
>
> Harap diingat bahwa mungkin akan butuh beberapa saat untuk menerima jawaban atas permintaan Anda. Simpan halaman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah petunjuk ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

### instagram

> Ikuti petunjuk di [ “Akun Samaran”](https://help.instagram.com/446663175382270) untuk meminta akun yang berkedok sebagai Anda itu dihapus.
>
> Harap diingat bahwa mungkin akan butuh beberapa saat untuk menerima jawaban atas permintaan Anda. Simpan halaman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah petunjuk ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)


### fake-website

> Periksa apakah situs web ini dikenal berbahaya dengan cara mencari URL-nya di layanan daring berikut:

> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)
> - [ThreatCrowd](https://www.threatcrowd.org/)

Apakah domain tersebut diketahui berbahaya?

 - [Yes](#malicious-website)
 - [No](#non-malicious-website)

### malicious-website

> Laporkan URL tersebut ke Google Safe Browsing dengan mengisi formulir [“Laporkan perangkat lunak berbahaya”](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Harap diingat bahwa mungkin perlu beberapa saat untuk memastikan bahwa laporan Anda berhasil. Sementara itu, Anda dapat melanjutkan ke langkah selanjutnya yaitu mengirim permintaan penghapusan ke penyedia hosting dan pencatat domain, atau simpanlah laman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah langkah ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#non-malicious-website)


### non-malicious-website

> Anda dapat mencoba melaporkan situs web tersebut ke penyedia hosting atau pencatat domain untuk meminta penghapusannya.
>
> Jika situs web yang ingin Anda laporkan menggunakan konten Anda, satu hal yang mungkin perlu Anda buktikan adalah bahwa Anda adalah pemilik sah dari konten aslinya. Anda dapat menunjukkan hal ini dengan memperlihatkan kontrak asli Anda dengan pencatat domain dan/atau penyedia hosting, tapi Anda juga dapat melakukan pencarian di [Wayback Machine](https://archive.org/web/), guna mencari URL situs web Anda dan situs web yang palsu. Jika kedua situs web tersebut telah terindeks di sana, Anda akan menemukan riwayat yang memungkinkan untuk memperlihatkan bahwa situs web Anda telah ada sebelum situs web palsu tersebut dipublikasikan.
>
> Untuk mengirimkan permintaan penghapusan, Anda juga akan perlu untuk mengumpulkan informasi tentang situs web palsu tersebut:
>
> - Buka layanan [Network Tools' NSLookup service](https://network-tools.com/nslookup/) dan cari tahu alamat (bisa lebih dari satu) IP dari situs web palsu dengan cara memasukkan URL-nya di formulir pencarian.
> - Tulislah alamat IP-nya.
> - Buka layanan [Domain Tools' Whois Lookup service](https://whois.domaintools.com/) dan carilah domain serta alamat IP dari situs web palsu tersebut.
> - Catat nama dan alamat email laporan penyalahgunaan milik penyedia hosting dan layanan domain. Jika hal itu termasuk dalam hasil pencarian Anda, catat juga nama pemilik situs webnya.
> - Kirim pesan ke penyedia hosting dan pencatat domain dari situs web palsu tersebut untuk meminta penghapusannya. Dalam pesan tersebut, sertakan informasi mengenai alamat IP, URL, dan pemilik situs web yang berkedok sebagai Anda tersebut, serta alasan mengapa situs tersebut merugikan.
> - Anda dapat menggunakan [template dari Access Now Helpline untuk melaporkan situs web yang dikloning ke penyedia hosting](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) untuk mengirim pesan ke penyedia hosting.
> - Anda dapat menggunakan [template dari Access Now Helpline untuk melaporkan peniruan identitas atau pengkloningan ke penyedia domain](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) untuk mengirim pesan ke registrar domain.
>
> Harap diingat bahwa mungkin akan membutuhkan beberapa saat untuk menerima balasan dari permintaan Anda. Simpan halaman ini pada bookmark Anda dan kembalilah ke alur kerja ini beberapa hari kemudian.

Apakah petunjuk ini berhasil?

- [Ya](#resolved_end)
- [Tidak](#web-protection_end)


### spoofed-email1

> Karena alasan teknis, mengautentikasi email cukup sulit dilakukan. Ini juga yang menjadi alasan mengapa sangat mudah membuat alamat pengirim palsu dan email palsu.

Apakah identitas Anda ditiru melalui alamat email, atau email yang mirip, misalnya dengan nama pengguna yang sama, tapi dengan domain yang berbeda?

- [Identitas saya dipalsukan melalui alamat email saya](#spoofed-email2)
- [Identitas saya ditiru melalui alamat email yang mirip](#similar-email)


### spoofed-email2

> Orang yang meniru identitas Anda mungkin telah meretas akun email Anda. Untuk menyingkirkan kemungkinan ini, cobalah untuk mengganti kata sandinya.

Apakah Anda dapat mengganti kata sandi Anda?

- [Ya](#spoofed-email3)
- [Tidak](#hacked-account)

### hacked-account

Jika Anda tidak bisa mengganti kata sandi Anda, mungkin akun email Anda telah disusupi.

- Anda dapat mengikuti [alur kerja “Saya tidak bisa mengakses akun saya”](../../../account-access-issues) untuk memecahkan masalah ini.

### spoofed-email3

> Email palsu tersusun dari pesan elektronik dengan alamat pengirim palsu. Pesan tersebut tampak seperti berasal dari seseorang atau sumber lain selain dari sumber sebenarnya.
>
> Email palsu umum ditemui dalam kampanye phishing dan spam karena orang lebih cenderung membuka email ketika mereka pikir email itu berasal dari sumber yang sah dan terpercaya.
>
> Jika seseorang memalsukan email Anda, Anda harus memberi tahu kontak Anda untuk memperingatkan mereka tentang bahaya phishing (lakukan dari akun email, profil, atau situs web yang sepenuhnya di bawah kendali Anda).
>
> Jika Anda mengira bahwa pemalsuan identitas ini ditujukan untuk phishing atau maksud jahat lainnya, Anda mungkin juga perlu membaca bagian [“Saya menerima pesan mencurigakan”.](../../../suspicious_messages).

Apakah email berhenti setelah Anda mengganti kata sandi akun email Anda?

- [Ya](#compromised-account)
- [Tidak](#secure-comms_end)


### compromised-account

> Mungkin akun Anda telah diretas oleh seseorang yang menggunakannya untuk mengirimkan email untuk menyamar sebagai Anda. Karena akun Anda telah disusupi, Anda mungkin juga ingin membaca bagian [“Saya kehilangan akses ke akun saya”](../../../account-access-issues/).

Apakah bagian ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#account_end)


### similar-email

> Jika si peniru identitas menggunakan alamat email yang serupa dengan milik Anda tetapi dengan domain atau nama pengguna yang berbeda, sebaiknya peringatkan kontak Anda mengenai upaya peniruan identitas Anda ini (lakukan dari akun email, profil, atau situs web yang sepenuhnya di bawah kendali Anda).
>
> Anda mungkin juga ingin membaca [bagian “Saya menerima pesan mencurigakan”](../../../suspicious-messages), karena peniruan identitas ini mungkin ditujukan untuk phishing.

Apakah tips ini membantu menyelesaikan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#secure-comms_end)


### PGP

Apakah menurut Anda kunci PGP pribadi Anda mungkin telah disusupi, misalnya karena Anda kehilangan kendali atas perangkat di tempat penyimpanannya?

- [Ya](#PGP-compromised)
- [Tidak](#PGP-spoofed)

### PGP-compromised

Apakah Anda masih memiliki akses ke kunci pribadi Anda?

- [Ya](#access-to-PGP)
- [Tidak](#lost-PGP)

### access-to-PGP

> - Cabut kunci Anda.
>     - [Petunjuk pencabutan di Enigmail](https://www.enigmail.net/index.php/en/user-manual/key-management#Revoking_your_key_pair)
> - Buat pasangan kunci baru dan mintalah untuk ditandatangani oleh orang yang Anda percayai.
> - Berkomunikasilah melalui kanal terpercaya yang Anda kendalikan, seperti Signal atau [perangkat yang terenkripsi secara end-to-end lainnya](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools), beritahu kontak Anda bahwa Anda telah mencabut kunci Anda dan membuat yang baru.

Apakah Anda membutuhkan lebih banyak bantuan untuk memecahkan masalah Anda?

- [Ya](#secure-comms_end)
- [Tidak](#resolved_end)


### lost-PGP

Apakah Anda memiliki sertifikat pencabutan (_revocation certificate_)?

- [Ya](#access-to-PGP)
- [Tidak](#no-revocation-cert)


### no-revocation-cert

> - Buat pasangan kunci baru dan mintalah untuk ditandatangani oleh orang yang Anda percayai.
> - Beritahu kontak Anda melalui kanal terpercaya yang Anda kendalikan, misalnya Signal atau [end-to-end encrypted tool](perangkat yang terenkripsi secara end-to-end lainnya), bahwa mereka harus menggunakan kunci baru Anda dan berhenti menggunakan yang lama.

Apakah Anda membutuhkan lebih banyak bantuan untuk memecahkan masalah Anda?

- [Ya](#secure-comms_end)
- [Tidak](#resolved_end)

### PGP-spoofed

Apakah kunci Anda ditandatangani oleh orang yang terpercaya?

- [Ya](#signed-key)
- [Tidak](#non-signed-key)

### signed-key

> - Beritahu kontak Anda melalui kanal terpercaya yang Anda kendalikan, misalnya Signal atau  [end-to-end encrypted tool](perangkat yang terenkripsi secara end-to-end lainnya), bahwa seseorang sedang mencoba meniru identitas Anda. Beritahu mereka bahwa mereka dapat mengenali kunci Anda yang sebenarnya berdasarkan (1) tanda tangan oleh kontak terpercaya dan/atau (2) [sidik jari (_fingerprint_)](https://help.gnome.org/users/seahorse/stable/misc-key-fingerprint.html.en) dari kunci asli Anda.

Apakah Anda membutuhkan lebih banyak bantuan untuk memecahkan masalah Anda?

- [Ya](#secure-comms_end)
- [Tidak](#resolved_end)

### non-signed-key

> - Minta kunci Anda [ditandatangani](https://communitydocs.accessnow.org/243-PGP_keysigning.html#comments) oleh orang yang Anda percaya.
> - Beritahu kontak Anda melalui kanal terpercaya yang Anda pegang kendalinya, seperti Signal atau [perangkat yang terenkripsi secara end-to-end](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools), lainnya, bahwa seseorang sedang mencoba meniru identitas Anda. Beritahu mereka bahwa mereka dapat mengenali kunci Anda yang sebenarnya berdasarkan (1) tanda tangan oleh kontak terpercaya dan/atau (2) [sidik jari](https://help.gnome.org/users/seahorse/stable/misc-key-fingerprint.html.en) dari kunci asli Anda.

Apakah Anda membutuhkan lebih banyak bantuan untuk memecahkan masalah Anda?

- [Ya](#secure-comms_end)
- [Tidak](#resolved_end)

### other-website

> Jika identitas Anda ditiru pada sebuah situs web, hal pertama yang perlu Anda lakukan adalah mengetahui di mana situs web tersebut dihosting, siapa yang mengelolanya, dan siapa yang menyediakan nama domainnya. Pencarian ini bertujuan untuk mencari cara terbaik untuk meminta penghapusan konten berbahaya.
>
> Sebelum Anda melanjutkan penyelidikan, jika Anda adalah warga negara Uni Eropa maka Anda dapat meminta Google untuk menghapus situs web tersebut dari hasil penelusuran atas nama Anda.

Apakah Anda seorang warga negara Uni Eropa?

- [Ya](#EU-privacy-removal)
- [Tidak](#doxing-question)


### EU-privacy-removal

> Isilah [formulir Permintaan Penghapusan Informasi Pribadi dari Google](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&visit_id=637202230061146146-20083139&rd=1) untuk menghapus situs web tersebut dari hasil penelusuran atas nama Anda di Google.  
>
> Yang Anda butuhkan adalah:
>
> - Salinan digital dari dokumen pengenal (jika Anda mengirimkan permintaan ini atas nama orang lain, Anda harus menyediakan dokumen pengenal untuk mereka)
> - URL konten yang mengandung informasi pribadi yang ingin Anda hapus
> - Untuk tiap URL yang Anda sediakan, Anda harus menjelaskan:
>     1. Bagaimana informasi pribadi yang disebutkan di atas berkaitan dengan orang yang atas namanya permintaan ini dibuat
>     2. Mengapa Anda merasa informasi pribadi tersebut harus dihapus
>
> Harap perhatikan jika Anda telah masuk ke Akun Google Anda, Google akan mengaitkan kiriman Anda dengan akun tersebut.
>
> Setelah mengirimkan formulir ini, Anda harus menunggu balasan dari Google untuk memverifikasi bahwa situs web tersebut telah dihapus dari halaman penelusuran.

Apakah Anda ingin mengajukan permintaan penghapusan untuk menghapus konten yang meniru identitas Anda dari situs web tersebut?

- [Ya](#doxing-question)
- [Tidak, saya ingin mendapatkan bantuan](#account_end)

### doxing-question

Apakah pihak peniru mempublikasikan informasi pribadi, atau video intim, atau foto Anda?

- [Ya](../../../harassed-online/questions/doxing-web)
- [Tidak](#fake-website)

### app1

> Jika seseorang menyebarkan salinan berbahaya dari aplikasi Anda atau perangkat lunak lainnya, sebaiknya lakukan komunikasi publik guna memperingatkan para pengguna untuk hanya mengunduh versi resminya.
>
> Anda juga harus melaporkan aplikasi berbahaya tersebut dan meminta penghapusannya.

Di mana salinan berbahaya aplikasi Anda didistribusikan?

- [Github](#github)
- [Gitlab.com](#gitlab)
- [Google Play Store](#playstore)
- [Apple App Store](#apple-store)
- [Situs web lain](#fake-website)

### github

> Jika perangkat lunak yang berbahaya tersebut dihosting di Github, baca [Panduan Github untuk Mengirim Pemberitahuan Penghapusan Digital Millennium Copyright Act (DMCA)](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) untuk menurunkan konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menunggu respon dari permintaan Anda. Simpan laman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah tips ini membantu memecahkan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)

### gitlab

> Jika perangkat lunak yang berbahaya tersebut dihosting di Gitlab.com, baca [persyaratan permintaan penurunan Digital Millennium Copyright Act (DMCA) milik Gitlab](https://about.gitlab.com/handbook/dmca/) untuk menurunkan konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menunggu respon dari permintaan Anda. Simpan laman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.\

Apakah tips ini membantu memecahkan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)


### playstore

> Jika aplikasi berbahaya tersebut dihosting di Google Play Store, ikuti langkah-langkah yang ada di [“Menghapus Konten dari Google”](https://support.google.com/legal/troubleshooter/1114905) untuk menghapus konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menunggu respon dari permintaan Anda. Simpan laman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah tips ini membantu memecahkan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)



### apple-store

> Jika aplikasi berbahaya tersebut dihosting di App Store, isilah [formulir “Sengketa Konten Apple App Store”](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=en) untuk menghapus konten yang melanggar hak cipta.
>
> Mungkin butuh beberapa saat untuk menunggu respon dari permintaan Anda. Simpan laman ini di bookmark Anda dan kembalilah ke alur kerja ini beberapa hari lagi.

Apakah tips ini membantu memecahkan masalah Anda?

- [Ya](#resolved_end)
- [Tidak](#app_end)



### physical-sec_end

> Jika Anda mengkhawatirkan keamanan fisik Anda, harap hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=physical_sec)


### account_end

> Jika Anda masih mengalami pemalsuan identitas atau akun Anda masih disusupi, harap hubungi organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=account&services=legal)


### app_end

> Jika permintaan penghapusan Anda masih belum berhasil, Anda bisa mencoba menghubungi organisasi di bawah ini untuk bantuan lebih lanjut.

:[](organisations?services=account&services=legal)

### web-protection_end

> If your takedown requests have not been successful, you can try reaching out to the organizations below for further support.

:[](organisations?services=web_protection)

### secure-comms_end

> Jika Anda butuh bantuan atau rekomendasi mengenai phishing, keamanan dan enkripsi email, serta komunikasi aman secara umum, Anda dapat menghubungi organisasi-organisasi berikut:

:[](organisations?services=secure_comms)


### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital (Digital First Aid Kit, DFAK) ini bermanfaat. Silakan berikan masukan melalui [email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Untuk mencegah upaya lebih jauh dari pemalsuan identitas Anda, bacalah tips di bawah ini.

### final_tips

- Buat kata sandi yang kuat, rumit, dan unik untuk semua akun Anda.
- Pertimbangkan untuk menggunakan pengelola kata sandi (_password manager_) untuk membuat dan menyimpan kata sandi, sehingga Anda dapat menggunakan berbagai kata sandi yang berbeda pada situs dan layanan yang berbeda tanpa harus menghafalkannya.
- Aktifkan verifikasi 2 langkah (2FA) untuk akun-akun terpenting Anda. 2FA menawarkan keamanan akun yang lebih baik dengan mengharuskan untuk menggunakan lebih dari satu metode untuk masuk ke akun Anda. Ini menunjukkan bahwa meski seseorang mendapatkan kata sandi utama Anda, mereka tidak bisa mengakses akun Anda kecuali mereka juga memiliki ponsel Anda atau sarana verifikasi sekunder lainnya.
- Verifikasi profil Anda di platform jejaring sosial. Beberapa platform menawarkan fitur untuk memverifikasi identitas Anda dan mengaitkannya dengan akun Anda.
- Petakan keberadaan online Anda. Self-doxing mengeksplorasi kecerdasan open source pada diri sendiri untuk mencegah pelaku kejahatan menemukan dan menggunakan informasi ini untuk meniru identitas Anda.
 - Pasang Google Alerts. Anda bisa mendapatkan email saat hasil penelusuran baru tentang suatu topik muncul di Google Search. Misalnya, Anda bisa mendapatkan informasi mengenai penyebutan nama Anda atau nama organisasi/kolektif Anda.
- Tangkap gambar laman web Anda sebagaimana adanya sekarang untuk digunakan sebagai bukti di masa yang akan datang. Jika situs web Anda mengizinkan perayap (_crawlers_), Anda bisa menggunakan the Wayback Machine yang ditawarkan oleh archive.org. Kunjungi [Internet Archive Wayback Machine](https://archive.org/web/), masukkan nama situs web Anda di bagian bawah header “Simpan Laman Sekarang”, dan klik tombol “Simpan Laman Sekarang”.

#### resources

- [Access Now Helpline Community Documentation: Choosing a password manager](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Access Now: SMS-based two step authentication (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Archive.org: Archive your website](https://archive.org/web/)
- [Security Self-Defense: Create strong and unique passwords](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Security Self-Defense: Animated Overview using password managers](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Security Self-Defense: How to use KeePassXC - a secure open source password manager](https://ssd.eff.org/en/module/how-use-keepassxc)
