---
layout: page
title: "Saya Kehilangan Perangkat Saya"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, past DFAK contributors
language: id
summary: "Saya kehilangan perangkat saya, apa yang harus saya lakukan?"
date: 2020-11
permalink: /id/topics/lost-device
parent: Home
---

# Saya kehilangan perangkat saya

Apakah perangkat Anda hilang? Apakah perangkat Anda dicuri atau disita oleh pihak ketiga?

Dalam situasi seperti ini, penting halnya untuk segera mengambil langkah untuk mengurangi risiko orang lain mengakses akun, kontak, dan informasi personal Anda.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa pertanyaan dasar sehingga Anda dapat menilai bagaimana cara mengurangi kemungkinan bahaya yang berkaitan dengan kehilangan perangkat.

## Workflow

### question-1

Apakah perangkatnya masih dalam kondisi hilang?

 - [Ya, perangkatnya hilang](#device-missing)
 - [Tidak, perangkatnya telah dikembalikan kepada saya](#device-returned)

### device-missing

> Ada baiknya memikirkan perlindungan keamanan apa yang dimiliki perangkat tersebut:
>
> * Apakah akses ke perangkat tersebut dilindungi oleh kata sandi atau langkah keamanan lainnya?
> * Apakah perangkat tersebut memiliki enkripsi keamanan yang aktif?
> * Bagaimana keadaan perangkat Anda saat hilang - Apakah dalam kondisi masih login? Apakah perangkatnya aktif tetapi terkunci dengan kata sandi? Apakah sedang dalam mode tidur atau hibernasi? Sepenuhnya dimatikan?

Dengan pertimbangan-pertimbangan tersebut, Anda dapat lebih memahami seberapa besar kemungkinan orang lain mendapatkan akses ke konten yang ada di perangkat Anda.

- [Mari hapus akses perangkat tersebut ke akun saya](#accounts)

### accounts

> Buat daftar semua akun yang dapat diakses oleh perangkat ini. Bisa berupa akun email, media sosial, pesan, kencan, dan perbankan, serta akun-akun yang mungkin menggunakan perangkat tersebut untuk autentikasi sekunder.
>
> Untuk akun apapun yang bisa diakses oleh perangkat tersebut (misalnya email, media sosial, atau akun web), Anda harus menghapus otorisasi perangkat tersebut terhadap akun-akun terkait. Hal tersebut bisa dilakukan dengan cara masuk ke akun Anda dan menghapus perangkat tersebut dari daftar perangkat yang memiliki izin.
>
> * [Akun Google](https://myaccount.google.com/device-activity)
> * [Akun Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [Akun iCloud](https://support.apple.com/en-us/HT205064)
> * [Akun Twitter](https://twitter.com/settings/sessions)
> * [Akun Yahoo](https://login.yahoo.com/account/activity)

Begitu Anda selesai menghapus tautan akun-akun Anda, mari amankan kata sandi Anda yang mungkin ada di perangkat tersebut.

- [Baik, mari kita urus kata sandinya](#passwords)

### passwords

> Pikirkan kata sandi apa saja yang secara langsung disimpan di perangkat tersebut, atau browser apa yang menyimpan kata-kata sandi.
>
> Ubah kata sandi semua akun yang bisa diakses oleh perangkat yang hilang. Jika Anda tidak menggunakan pengelola kata sandi, sangat disarankan untuk menggunakannya demi membuat dan mengelola kata sandi yang kuat dengan lebih baik.

Setelah mengubah kata sandi akun-akun Anda di perangkat Anda, pikirkan apakah Anda menggunakan kata-kata sandi tersebut untuk akun lain - jika ya, harap ubah pula kata sandi tersebut.

- [Saya menggunakan beberapa kata sandi pada perangkat yang hilang di beberapa akun lain](#same-password)
- [Semua kata sandi saya yang mungkin disusupi berbeda satu sama lain](#2fa)

### same-password

Apakah Anda menggunakan kata sandi yang sama pada akun atau perangkat lain selain perangkat yang hilang? Jika iya, ubah kata sandi pada akun-akun tersebut karena akun-akun tersebut bisa jadi juga tersusupi.

- [Baik, sekarang saya sudah mengubah semua kata sandi terkait](#2fa)

### 2fa

> Mengaktifkan verifikasi 2 langkah pada akun-akun yang menurut Anda berisiko diakses orang lain dapat mengurangi kemungkinan akun-akun tersebut diakses oleh orang lain.
>
> Aktifkan verifikasi 2 langkah pada semua akun yang dapat diakses dari perangkat yang hilang. Harap perhatikan bahwa tidak semua akun mendukung verifikasi 2 langkah. Harap kunjungi [Two Factor Auth](https://2fa.directory/) untuk melihat daftar layanan dan platform yang mendukung verifikasi 2 langkah.

- [Saya telah mengaktifkan verifikasi 2 langkah pada akun-akun saya untuk pengamanan lebih lanjut. Saya ingin mencoba mencari atau menghapus perangkat saya](#find-erase-device)

### find-erase-device

> Pikirkan untuk apa Anda menggunakan perangkat ini - apakah ada informasi sensitif di perangkat ini, misalnya kontak, lokasi, atau pesan-pesan? Bisakah kebocoran data yang terjadi menjadi masalah bagi Anda, pekerjaan Anda, atau orang lain selain diri Anda?
>
> Dalam beberapa kasus, menghapus data pada perangkat milik tahanan dari jarak jauh dapat bermanfaat untuk mencegah data di dalamnya disalahgunakan dan digunakan untuk melawan mereka atau aktivis lainnya. Pada saat yang sama, hal ini mungkin menjadi masalah bagi pihak yang ditahan (terutama dalam situasi di mana penyiksaan dan penganiayaan bisa terjadi) - terutama jika pihak yang ditahan dipaksa untuk memberikan akses ke perangkat tersebut, data yang tiba-tiba menghilang dari perangkat tersebut dapat membuat otoritas penahanan semakin curiga. Jika Anda menghadapi situasi serupa, baca bagian [“Seseorang yang saya kenal telah ditangkap” di panduan ini](../../../../arrested).
>
> Penting pula untuk diingat bahwa di beberapa negara penghapusan jarak jauh bisa jadi dianggap bertentangan dengan hukum, karena dapat ditafsirkan sebagai penghancuran barang bukti.
>
> Jika Anda sudah memahami konsekuensi langsung dan hukum bagi semua orang yang terlibat, Anda dapat melanjutkan dengan mencoba menghapus perangkat tersebut dari jarak jauh, dengan mengikuti instruksi untuk sistem operasi Anda:
>
> * [perangkat Android](https://support.google.com/accounts/answer/6160491?hl=id)
> * [iPhone atau Mac](https://www.icloud.com/#find)
> * [Perangkat iOS (iPhone dan iPad) yang menggunakan iCloud](https://support.apple.com/kb/ph2701)
> * [Perangkat Windows](https://support.microsoft.com/en-us/help/11579/microsoft-account-find-and-lock-lost-windows-device)
> * [Ponsel Blackberry](https://docs.blackberry.com/en/endpoint-management/blackberry-uem/12_9/blackberry-uem-self-service-user-guide/amo1375908155714)
> * Untuk perangkat Windows dan Linux, Anda mungkin sudah memasang perangkat lunak (seperti perangkat lunak anti-pencurian atau anti-virus) yang memungkinkan Anda untuk menghapus data dan riwayat perangkat Anda dari jarak jauh. Jika demikian, gunakanlah.

Baik Anda sudah berhasil menghapus informasi di perangkat Anda atau belum, sebaiknya beritahu kontak Anda.

- [Lanjutkan ke langkah selanjutnya untuk menemukan tips tentang cara memberitahu kontak Anda mengenai hilangnya perangkat Anda.](#inform-network)

### inform-network

> Selain akun Anda sendiri, perangkat Anda mungkin juga memiliki informasi tentang orang lain di dalamnya. Hal ini bisa termasuk kontak, komunikasi dengan orang lain, dan grup pesan.
>
> Saat mempertimbangkan memberitahu jaringan dan komunitas Anda tentang perangkat yang hilang, gunakan [prinsip pengurangan bahaya](../../../../arrested#harm-reduction) untuk memastikan bahwa dengan menghubungi orang lain Anda tidak menempatkan mereka pada risiko yang lebih besar.

Beritahu jaringan Anda mengenai perangkat yang hilang tersebut. Hal ini bisa dilakukan secara pribadi dengan kontak utama yang berisiko tinggi, atau memposting secara publik daftar akun yang berpotensi disusupi di situs web Anda atau di akun media sosial, jika Anda merasa itu patut dilakukan.

- [Saya telah memberitahu kontak saya](#review-history)


### review-history

> Jika memungkinkan, tinjau riwayat koneksi/aktivitas akun dari semua akun yang terhubung ke perangkat tersebut. Periksa apakah akun Anda digunakan pada saat Anda sedang tidak online, atau jika akun Anda diakses dari lokasi atau alamat IP yang tidak dikenal.
>
> Anda dapat meninjau aktivitas Anda di beberapa penyedia layanan populer di bawah ini:
>
> * [Akun Google](https://myaccount.google.com/device-activity)
> * [Akun Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [Akun iCloud](https://support.apple.com/en-us/HT205064)
> * [Akun Twitter](https://twitter.com/settings/sessions)
> * [Akun Yahoo](https://login.yahoo.com/account/activity)

Apakah Anda sudah meninjau riwayat koneksi Anda?

- [Sudah, dan tidak ada yang mencurigakan](#check-settings)
- [Sudah, dan ada beberapa aktivitas mencurigakan](../../../account-access-issues/)

### check-settings

> Periksa pengaturan akun dari semua akun yang terhubung dengan perangkat tersebut. Apakah ada yang berubah? Untuk akun email, periksalah penerusan otomatis, kemungkinan perubahan pada alamat email atau nomor telepon cadangan/reset, sinkronisasi ke perangkat berbeda, termasuk telepon, komputer, atau tablet, dan izin ke aplikasi atau perizinan akun lainnya.
>
> Ulangi peninjauan riwayat aktivitas akun setidaknya seminggu sekali selama sebulan, untuk memastikan akun Anda tetap tidak menunjukkan aktivitas yang mencurigakan.

- [Riwayat aktivitas akun saya menunjukkan aktivitas yang mencurigakan](../../../account-access-issues/)
- [Riwayat aktivitas akun saya tidak menunjukkan aktivitas mencurigakan apapun, namun saya akan terus meninjaunya dari waktu ke waktu](#resolved_end)


### device-returned

> Jika perangkat Anda hilang, diambil oleh pihak ketiga, atau harus diserahkan di area perbatasan, tetapi Anda telah memilikinya kembali, berhati-hatilah karena Anda tidak tahu siapa yang telah memiliki akses ke perangkat tersebut. Tergantung pada tingkat risiko yang Anda hadapi, sebaiknya Anda memperlakukan perangkat tersebut seakan-akan perangkat tersebut kini tak terpercaya atau telah tersusupi.

> Ajukan pertanyaan berikut kepada diri Anda sendiri dan perkirakan risiko perangkat Anda telah tersusupi:

> * Berapa lama perangkat tersebut berada di luar kendali Anda?
> * Siapa yang berpotensi memiliki akses terhadapnya?
> * Mengapa mereka mau mengaksesnya?
> * Apakah ada tanda-tanda bahwa perangkat tersebut telah diutik secara fisik?

> Jika Anda tidak lagi mempercayai perangkat Anda, pertimbangkan untuk menghapus dan memasang ulang perangkat Anda, atau menggunakan perangkat baru.

Apakah Anda memerlukan bantuan untuk mendapatkan perangkat pengganti?

- [Ya](#new-device_end)
- [Tidak](#resolved_end)


### accounts_end

Jika Anda telah kehilangan akses ke akun-akun Anda atau merasa seseorang mungkin memiliki aksesnya, harap kontak organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=account)

### new-device_end

Jika Anda membutuhkan dukungan keuangan eksternal untuk mendapatkan perangkat pengganti, harap kontak organisasi yang dapat membantu Anda di bawah ini.

:[](organisations?services=equipment_replacement)

### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital (Digital First Aid Kit, DFAK) ini bermanfaat. Silakan berikan masukan [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Berikut adalah serangkaian tips untuk menanggulangi risiko kebocoran data dan akses tidak sah ke akun dan informasi Anda:

- Jangan pernah tinggalkan perangkat Anda tanpa pengawasan. Jika Anda tidak bisa membawanya bersama Anda, matikan dan simpanlah di tempat yang aman.
- Aktifkan enkripsi full-disk.
- Gunakan kata sandi yang kuat untuk mengunci perangkat Anda.
- Aktifkan fitur Find/Erase My Phone kapanpun memungkinkan, tapi perlu diingat bahwa fitur ini bisa digunakan untuk melacak Anda atau menghapus perangkat Anda, jika akun Anda yang terkait (Gmail/iCloud) telah disusupi.

#### sumber informasi

* [Access Now Helpline Community Documentation: Tips on how to enable full-disk encryption](https://communitydocs.accessnow.org/166-Full-Disk_Encryption.html)
* [Security in a Box: Tactics on securing sensitive files](https://securityinabox.org/en/guide/secure-file-storage/)
* Pertimbangkan menggunakan perangkat lunak anti-pencurian seperti [Prey](https://preyproject.com)
