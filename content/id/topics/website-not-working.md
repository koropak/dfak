---
layout: page
title: "Situs web saya tidak bisa diakses, apa yang terjadi?"
author: Rarenet
language: id
summary: "Ancaman yang dihadapi oleh banyak LSM, media independen, dan blogger adalah terbungkamnya suara mereka karena situs web mereka tidak bisa diakses atau dirusak."
date: 2020-11
permalink: /id/topics/website-not-working/
parent: /id/
---

# Situs web saya tidak bisa diakses, apa yang terjadi?

Ancaman yang dihadapi oleh banyak LSM, media independen, dan blogger adalah terbungkamnya suara mereka karena situs web mereka down (tidak bisa diakses) atau dirusak. Ini adalah masalah yang membuat frustasi dan dapat disebabkan oleh berbagai macam hal, seperti pemeliharaan situs web yang buruk, hosting yang tidak andal, [script-kiddies](https://en.wikipedia.org/wiki/Script_kiddie), serangan ‘denial of service’, atau pengambilalihan situs web. Di bagian ini, Pertolongan Pertama Darurat Digital akan memandu Anda melalui beberapa langkah dasar untuk mendiagnosis kemungkinan masalah dengan menggunakan materi dari [My Website is Down](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Penting untuk diketahui bahwa ada banyak alasan mengapa situs web Anda tidak bisa diakses. Hal tersebut dapat berkisar dari masalah teknis di perusahaan yang menghosting situs web tersebut atau Sistem Manajemen Konten (CMS) seperti Joomla atau WordPress yang belum diperbarui. Menemukan masalahnya dan solusi yang memungkinkan untuk permasalahan situs web Anda bisa jadi rumit. Sebaiknya **hubungi webmaster dan host situs web Anda** setelah mendiagnosis masalah-masalah umum di bawah ini. Jika tidak ada satupun dari pilihan di bawah ini yang sesuai dengan Anda, carilah [bantuan dari organisasi yang Anda percaya](../website_down_end).

## Consider

Sebagai permulaan, pertimbangkan:

- Siapa yang membangun situs web Anda? Apakah mereka bisa membantu?
- Apakah situs web tersebut dibangun dengan WordPress atau platform CMS populer lainnya?
- Siapa penyedia hosting web Anda? Jika Anda tidak tahu, Anda dapat menggunakan [layanan online WHOIS](http://www.whoishostingthis.com/) untuk membantu.

## Workflow

### error-message

Apakah Anda melihat pesan error?

 - [Ya, saya melihat pesan error](#error-message-yes)
 - [Tidak](#hosting-message)


### hosting-message

Apakah Anda melihat pesan dari penyedia hosting web Anda?

- [Ya, saya melihat pesan dari penyedia hosting saya](#hosting-message-yes)
- [Tidak](#site-not-loading)


### site-not-loading

Apakah situs Anda tidak terbuka sama sekali?

- [Ya, situsnya tidak terbuka sama sekali](#site-not-loading-yes)
- [Tidak, situsnya terbuka](#hosting-working)


### hosting-working

Apakah situs web penyedia hosting Anda bisa diakses, tapi situs web Anda tidak?

- [Ya, saya bisa mengakses situs web penyedia hosting saya](#hosting-working-yes)
- [Tidak](#similar-content-censored)


### similar-content-censored

Apakah Anda bisa mengunjungi situs lain yang kontennya sama dengan situs Anda

- [Saya juga tidak bisa mengunjungi situs lain dengan konten serupa.](#similar-content-censored-yes)
- [Situs lain bekerja dengan baik. Saya hanya tidak bisa mengunjungi situs saya.](#loading-intermittently)


### loading-intermittently

Apakah pemuatan situs Anda terputus-putus, atau lebih lambat dari biasanya?

- [Ya, situs saya terbuka dengan terputus-putus atau lambat](#loading-intermittently-yes)
- [Tidak, situs web saya terbuka seperti biasa, tapi mungkin telah diretas](#website-defaced)


### website-defaced

Apakah situs web Anda terbuka tetapi tampilan dan kontennya tidak seperti yang Anda harapkan?

- [Ya, konten/tampilan situs saya tidak seperti yang diharapkan](#defaced-attack-yes)
- [Tidak](#website-down_end)


### error-message-yes

> Bisa jadi ini adalah ***masalah perangkat lunak*** -- Anda harus memikirkan perubahan terbaru apapun yang mungkin telah Anda atau tim Anda lakukan, kemudian hubungi webmaster Anda. Mengirimi webmaster Anda tangkapan layar, tautan ke halaman yang bermasalah, dan pesan error apapun yang Anda temukan akan membantu mereka mencari tahu penyebab masalahnya. Anda juga dapat menyalin pesan error yang Anda dapatkan ke mesin pencari untuk mencari tahu apakah ada solusi mudahnya.

Apakah tips ini membantu?

- [Ya](#resolved_end)
- [Tidak](#website-down_end)


### hosting-message-yes

> Situs web Anda bisa dibuat offline karena alasan hukum, [copyright](https://www.eff.org/issues/bloggers/legal/liability/IP), penagihan, atau alasan lainnya. Hubungi penyedia hosting Anda untuk detail lebih lanjut mengenai mengapa mereka menangguhkan hosting situs web Anda.

Has this helped?

- [Ya](#resolved_end)
- [Tidak, saya butuh bantuan hukum](#legal_end)
- [Tidak, saya butuh bantuan teknis](#website-down_end)


### site-not-loading-yes

> Perusahaan hosting Anda mungkin sedang bermasalah, yang mana dalam hal ini mungkin Anda mengalami ***masalah hosting***. Dapatkah Anda mengunjungi situs web perusahaan hosting Anda? Perhatikan, **bukan** bagian admin dari situs Anda, melainkan perusahaan atau organisasi yang bekerja dengan Anda untuk menghosting situs Anda.
>
> Cari atau selidiki “status” blog (misalnya status.dreamhost.com), dan juga carilah di twitter.com untuk melihat diskusi dari pengguna lain mengenai downtime di perusahaan hosting tersebut - pencarian sederhana seperti “(nama perusahaan) down” seringkali dapat mengungkap apakah banyak orang mengalami masalah yang sama.

Apakah tips ini membantu?

- [Ya](#resolved_end)
- [Tidak, saya butuh bantuan hukum](#hosting-working-yes)
- [Tidak, saya butuh bantuan teknis](#website-down_end)


### hosting-working-yes

> Periksa apakah situs webnya berfungsi dengan [Down for Everyone or Just Me](https://downforeveryoneorjustme.com/) - situs Anda mungkin berfungsi tapi Anda tidak bisa melihatnya.
>
> Jika situs Anda berfungsi tapi Anda tidak bisa melihatnya, bisa jadi itu adalah ***masalah jaringan***: koneksi internet Anda bisa jadi bermasalah atau memblokir akses ke situs Anda.

Apakah Anda membutuhkan bantuan lebih lanjut?

- [Tidak](#resolved_end)
- [Ya, saya membutuhkan bantuan untuk memulihkan koneksi internet saya](#website-down_end)
- [Ya, ini bukanlah masalah jaringan dan situs web saya tidak bisa diakses siapapun](#similar-content-censored)


### similar-content-censored-yes

> Coba kunjungi situs-situs web dengan konten yang serupa dengan situs web Anda. Coba juga untuk menggunakan [Tor](https://www.torproject.org/projects/gettor.html), [Psiphon](https://psiphon.ca/en/index.html), atau VPN untuk mengakses situs Anda.
>
> Jika Anda bisa mengunjungi situs Anda melalui Tor, Psiphon, atau VPN, Anda mengalami ***masalah sensor*** - situs Anda masih berfungsi bagi belahan dunia lainnya, tapi disensor di negara Anda sendiri.

Apakah Anda ingin melakukan sesuatu terkait sensor ini?

- [Ya, saya ingin melaporkan hal ini secara terbuka dan butuh bantuan untuk kampanye advokasi saya](#advocacy_end)
- [Ya, saya ingin mencari solusi untuk membuat situs web saya dapat diakses](#website-down_end)
- [Tidak](#resolved_end)


### loading-intermittently-yes

> Situs Anda mungkin kewalahan oleh jumlah dan kecepatan permintaan halaman yang diterimanya - ini adalah ***masalah kinerja***.
>
> Ini bisa jadi “baik” karena situs Anda populer dan hanya perlu beberapa perbaikan untuk merespon pembaca yang lebih banyak - periksa analisis situs Anda untuk melihat pola pertumbuhan jangka panjang. Hubungi webmaster atau penyedia hosting Anda untuk panduannya. Banyak platform blogging dan sistem pengelola konten (CMS) terkenal (Joomla, Wordpress, Drupal...) memiliki plugin untuk membantu proses cache situs web Anda secara lokal dan mengintegrasikan [Content Delivery Networks](https://en.wikipedia.org/wiki/Content_delivery_network), yang dapat memperbaiki kinerja dan ketahanan situs secara drastis. Solusi-solusi di bawah ini yang juga dapat membantu permasalahan kinerja.
>
> Jika Anda mengalami masalah kinerja yang parah, situs Anda mungkin telah menjadi korban dari serangan [**"distributed denial of service"**](https://ssd.eff.org/en/glossary/distributed-denial-service-attack) (atau DDoS). Ikuti langkah-langkah di bawah ini untuk menanggulangi serangan seperti itu:
>
> - Langkah 1: Hubungi orang yang terpercaya yang dapat membantu menangani situs web Anda (webmaster, orang yang membantu menyiapkan situs, staf internal, atau penyedia hosting).
>
> - Langkah 2: Bekerja samalah dengan perusahaan tempat Anda membeli nama domain dan ubahlah “Time to Live” atau TTL menjadi 1 jam (Anda bisa menemukan petunjuk mengenai cara melakukannya di situs web banyak penyedia layanan, seperti [Network Solutions](http://www.networksolutions.com/support/how-to-manage-advanced-dns-records/) atau [GoDaddy](http://support.godaddy.com/help/article/680/managing-dns-for-your-domain-names)). Hal ini dapat membantu Anda mengarahkan ulang situs Anda dengan lebih cepat begitu diserang (standarnya adalah 72 jam atau tiga hari). Pengaturan ini seringkali ditemukan di bagian “lanjutan” dari properties di domain Anda, kadang menjadi bagian dari SRV atau catatan layanan.
>
> - Step 3: Pindahkan situs Anda ke layanan penanggulangan DDoS. Sebagai permulaan:
>
>     - [Deflect.ca](https://deflect.ca/)
>     - [Project Shield dari Google](https://projectshield.withgoogle.com/landing)
>     - [Projek Galileo dari Cloudflare](https://www.cloudflare.com/galileo)
>
> Untuk daftar lengkap organisasi terpercaya yang dapat membantu menanggulangi serangan DDoS, kunjungi bagian terakhir dari alur kerja ini yang membahas tentang DDoS.  
>
> - Langkah 4: Begitu Anda mendapatkan kendali kembali, tinjau ulang kebutuhan Anda dan putuskan apakah Anda butuh penyedia hosting yang aman atau melanjutkan saja dengan layanan penanggulangan DDoS yang Anda miliki.

-  Untuk daftar lengkap organisasi terpercaya yang dapat menyediakan hosting aman, kunjungi [bagian terakhir dari alur kerja ini yang membahas tentang persoalan hosting web](#web-hosting_end).

### defaced-attack-yes

> Website defacement is a practice where an attacker replaces the content or the visual appearance of the website with their own content. These attacks are usually conducted by either exploiting vulnerabilities in unmaintained CMS platforms without the latest security updates or by using stolen hosting account usernames/passwords.
>
> - Langkah 1: Verifikasi bahwa ini adalah pengambilalihan berbahaya dari situs web Anda. Penerapan yang disayangkan tapi legal adalah membeli nama domain yang baru saja kedaluwarsa untuk ‘mengambil alih’ traffic yang dimiliki, untuk keperluan periklanan. Sangat penting halnya untuk menyimpan pembayaran untuk nama domain Anda dengan teratur.  
> - Langkah 2: Jika situs web Anda telah dirusak, pertama-tama dapatkanlah kembali kendali atas akun login situs web Anda dan atur ulang kata sandinya, lihat bagian [Pembajakan Akun](../../../account-access-issues) untuk membaca bantuan.
> - Langkah 3: Buat cadangan dari situs yang dirusak, yang nantinya dapat digunakan untuk menyelidiki kerusakannya.
> - Langkah 4: Nonaktifkan situs web Anda sementara waktu - gunakan landing page atau laman parked domain.
> - Langkah 5: Pastikan bagaimana situs Anda bisa diretas. Penyedia hosting Anda mungkin bisa membantu. Penyebab umumnya adalah ada bagian-bagian lama dari situs Anda yang menjalankan skrip/alat khusus, sistem pengelola konten yang kedaluwarsa, dan pemrograman khusus yang memiliki kelemahan pada keamanannya.
> - Langkah 6: Pulihkan situs asli Anda dengan menggunakan cadangan data. Jika Anda atau perusahaan hosting Anda sama-sama tidak memiliki cadangan data, Anda mungkin harus membangun ulang situs web Anda dari awal! Perhatikan juga bahwa jika satu-satunya cadangan data ada di penyedia hosting Anda, penyerang mungkin bisa menghapusnya saat mereka mengambil alih kendali situs Anda!

Apakah rekomendasi ini membantu?

- [Ya](#resolved_end)
- [Tidak](#website-down_end)


### website-down_end

> Jika Anda masih membutuhkan bantuan setelah semua pertanyaan yang Anda jawab, Anda dapat menghubungi organisasi terpercaya dan meminta bantuan.
>
> Sebelum Anda menghubungi mereka, harap tanyakan diri Anda sendiri pertanyaan-pertanyaan berikut ini:
>
> - Bagaimana struktur perusahaan/organisasi tersebut, dan bagaimana dijalankannya? Jenis pemeriksaan atau pelaporan seperti apa yang harus mereka lakukan, jika ada?
> - Pertimbangkan di negara apa mereka diakui secara hukum serta di mana mereka akan diminta untuk mematuhi aturan hukum dan permintaan hukum lainnya.
> - Log apa yang dibuat, dan berapa lama tersedianya?
> - Apakah ada batasan terkait jenis konten yang akan dihosting/diproksi oleh layanan tersebut, dan bisakah hal tersebut berdampak pada situs Anda?
> - Adakah batasan di negara-negara di mana mereka dapat menyediakan layanan?
> - Apakah mereka menerima bentuk pembayaran yang bisa Anda gunakan? Apakah Anda mampu membayar layanan mereka?
> - Komunikasi yang aman - Anda harus bisa log in dengan aman dan berkomunikasi dengan penyedia layanan secara privat.
> - Apakah ada opsi untuk verifikasi dua langkah, untuk meningkatkan keamanan akses administrator? Hal ini atau kebijakan akses aman yang terkait dapat membantu menurunkan ancaman bentuk serangan lainnya terhadap situs web Anda.
> - Jenis bantuan berkelanjutan seperti apa yang bisa Anda akses? Apakah ada biaya tambahan untuk bantuan tersebut, dan/atau akankah Anda menerima bantuan yang cukup jika Anda menggunakan layanan ‘gratis’?
> - Bisakah Anda menguji coba situs web Anda sebelum Anda melanjutkan melalui situs staging?

Berikut adalah daftar organisasi yang dapat membantu masalah Anda:

:[](organisations?services=web_protection)

### legal_end

> Jika situs web Anda tidak bisa diakses karena alasan hukum dan Anda membutuhkan bantuan hukum, harap hubungi organisasi yang dapat membantu:

:[](organisations?services=legal)

### advocacy_end

> Jika Anda menginginkan bantuan untuk meluncurkan kampanye melawan penyensoran, harap hubungi organisasi yang dapat membantu dengan upaya advokasi:

:[](organisations?services=advocacy)

### ddos_end

> Jika Anda membutuhkan bantuan untuk menanggulangi serangan DDoS terhadap situs web Anda, harap mengacu pada organisasi yang berspesialisasi pada penanggulangan DDoS:

:[](organisations?services=ddos)


### web-hosting_end

> Jika Anda mencari organisasi terpercaya untuk menghosting situs web Anda di server yang aman, silakan lihat daftar di bawah ini.
>
> Sebelum Anda menghubungi organisasi tersebut, harap pikirkan pertanyaan-pertanyaan berikut:
>
> - Apakah mereka menawarkan bantuan penuh dalam memindahkan situs Anda ke layanan mereka?
> - Apakah layanannya setara atau lebih baik dari host Anda yang sekarang, setidaknya untuk peralatan/layanan yang Anda gunakan? Hal-hal terpenting yang harus Anda periksa adalah:
>     - Pengelolaan dasbor, misalnya cPanel
>     - Akun email (berapa banyak, kuota, akses melalui SMTP, IMAP)
>     - Basis data (berapa banyak, tipe, akses)
>     - Akses jarak jauh melalui SFTP/SSH
>     - Dukungan untuk bahasa pemrograman (PHP, Perl, Ruby, akses cgi-bin…) atau CMS (Drupal, Joomla, Wordpress…) yang digunakan situs Anda

Berikut adalah daftar organisasi yang dapat membantu Anda dengan urusan hosting web:

:[](organisations?services=web_hosting)


### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital (Digital First Aid Kit, DFAK) ini bermanfaat. Silakan berikan masukan melalui [email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Data cadangan** - Selain layanan dan saran di bawah ini, ada baiknya Anda memastikan bahwa Anda memiliki cadangan data (yang Anda simpan di suatu tempat selain tempat yang sama di mana Anda menyimpan situs web Anda!). Banyak host dan platform situs web yang menyertakannya, tapi lebih baik Anda juga memiliki salinan offline tambahan.

- **Selalu perbarui perangkat lunak** - Jika Anda menggunakan Sistem Pengelolaan Konten (CMS) seperti WordPress atau Drupal, pastikan teknologi situs web Anda diperbarui ke perangkat lunak terbaru, terutama jika ada pembaruan keamanan.

- **Pemantauan** - Ada banyak layanan yang dapat senantiasa memeriksa situs Anda dan mengirim email atau pesan kepada Anda jika situs Anda tidak berfungsi. [Artikel Mashable ini](http://mashable.com/2010/04/09/free-uptime-monitoring/)  membuat daftar 10 layanan terpopuler. Waspadalah bahwa email atau nomor telepon yang Anda gunakan untuk memantau tentunya akan dikaitkan dengan pengelolaan situs web.

#### Resources

- [EFF: Keeping your site alive](https://www.eff.org/keeping-your-site-alive)
- [CERT.be: DDoS proactive and reactive measures](https://www.cert.be/en/paper/ddos-proactive-and-reactive-measures)
- [Sucuri: What is a DDoS Attack?](​​​​​​​https://sucuri.net/guides/what-is-a-ddos-attack/)
