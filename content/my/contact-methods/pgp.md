---
layout: page
title: PGP
author: mfc
language: my
summary: ဆက်သွယ်ရန် နည်းလမ်းများ
date: 2021-03
permalink: /my/contact-methods/pgp.md
parent: /my/
published: true
---

PGP (Pretty Good Privacy) နှင့် အများသူသုံးနိုင်သောအလားတူ ဝန်ဆောင်မှု GPG (Gnu Privacy Guard) များသည် အီးမေးလ်ဝန်ဆောင်မှုပေးသူ နှင့် အခြား ကြည့်ရှုနိုင်စွမ်းရှိသောအဖွဲ့အစည်းများက သင့် အီးမေးလ်အရာများကို မကြည့်ရှုနိုင်ရန် သင့်အီးမေးလ်များကို ကုဒ်ဖြင့်ပြောင်းလဲကာကွယ်ပေးပါသည်။ သို့သော်လည်း အစိုးရများနှင့် ဥပဒေစိုးမိုးရေးအေဂျင်စီများက ဆက်သွယ်မှုဖြစ်စဥ်ရှိမရှိကို  သိရှိနိုင်ပါသည်။ ဤအလားအလာများ မဖြစ်စေရန် သင့်ပုဂ္ဂိုလ်ရေးရာနှင့် မသက်ဆိုင်သော အခြားအီးမေးလ်လိပ်စာဖြင့် သင်မှပြောင်းသုံးနိုင်ပါသည်။

လေ့လာရန် - [Access Now ၏ အကူအညီလိုအပ်သော အသိုင်းအဝိုင်းအတွက် စာရွက်စာတမ်းများ - လုံခြုံသောအီးမေးလ်](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Mailvelope ဖြင့် အီးမေးလ်အား ကုဒ်ဖြင့်ပြောင်းလဲခြင်း - သုံးသူအသစ်အတွက်လမ်းညွှန်](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[လုံခြုံရေးကိရိယာများ - ပုဂ္ဂလိက အီးမေးလ်ဝန်ဆောင်မှုပေးသူများ](https://www.privacytools.io/providers/email/)
