---
layout: page
title: Skype
author: mfc
language: my
summary: Contact methods
date: 2021-03
permalink: /my/contact-methods/skype.md
parent: /my/
published: true
---

သင့် မက်ဆေ့ချ်အကြောင်းအရာများနှင့် အဖွဲ့အစည်းသို့ ဆက်သွယ်မှုပြုလုပ်ခဲ့ခြင်း အချက်အလက်တွေကို အစိုးရများနှင့် ဥပဒေစိုးမိုးရေးအေဂျင်စီများက ကြည့်ရှုသိရှိနိုင်ပါသည်။
