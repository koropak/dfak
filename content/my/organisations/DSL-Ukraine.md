---
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: တနင်္လာမှကြာသာပတေး ၊ 9am-5pm EET/EEST
response_time: ၂ ရက်
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

Digital Security Lab Ukraine သည် ISC Project ၏ ToT အစီအစဥ်မှ ဘွဲ့ရခဲ့သူ ၄ ဦးမှ ၂၀၁၇ ခုနှစ်တွင် တည်ထောင်ခဲ့သော Kyiv အခြေစိုက် အစိုးရမဟုတ်သော အဖွဲ့အစည်း ဖြစ်ပါသည်။ ကျွန်ုပ်တို့၏ ရည်ရွယ်ချက်သည် အင်တာနက်ပေါ်တွင် လူ့အခွင့်အရေးများကို အကောင်အထည်ဖော်ရန် ဖြစ်ပါသည်။ မည်ကဲ့သို့ ဆောင်ရွက်မည်ဆိုသည်မှာ ၁) NGO များနှင့် လွပ်လပ်သော မီဒီယာများ၏ စွမ်းဆောင်ရည်များကို မြှင့်တင်ပေးပြီး ၎င်းတို့၏ ဒီဂျစ်တယ် လုံခြုံရေးဆိုင်ရာ စိုးရိမ်မှုများကို ကာကွယ်ကိုင်တွယ်ဖြေရှင်းပေးခြင်း နှင့် ၂) ဒီဂျစ်တယ်အခွင့်အရေးကိစ္စရပ်များတွင် အစိုးရ နှင့် ကော်ပရိတ်မူဝါဒများသို့ သက်ရောက်မှုရှိစေခြင်း တို့ဖြစ်ပါသည်။
