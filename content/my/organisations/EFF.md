---
name: Electronic Frontier Foundation
website: https://www.eff.org
logo: eff-logo-lockup-black.png
languages: English, Español, Français
services: in_person_training, legal,  vulnerabilities_malware, forensic, advocacy
beneficiaries: journalists, hrds, cso, activists, lgbti, land, women, youth
hours: တနင်္လာကနေ သောကြာ ၊ 9am-5pm PST
response_time: အလုပ်လုပ်ရက် (၃) ရက်
contact_methods: email, pgp, phone, signal
email: info@eff.org
pgp_key: https://www.eff.org/files/2013/10/01/info-eff-org.txt.key
pgp_key_fingerprint: F2F2 1BB8 531E 9DC3 0D40 F68B 11A1 A9C8 4B18 732F
phone: +1-415-436-9333
signal: +1-510-243-8020
initial_intake: no
---

Electronic Frontier Foundation ၏ မစ်ရှင်မှာ နည်းပညာနှင့်​ ယှက်နွက်သော နိုင်ငံသားလွတ်လပ်ခွင့် နှင့် လူ့အခွင့်အရေးတို့ကို ကာကွယ်ခြင်းပင်ဖြစ်သည်။
