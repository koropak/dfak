---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: Segunda-Sexta, GMT+5.30
response_time: 2 horas durante o período de trabalho, até 24 horas em outros casos
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

A Tibetan Computer Emergency Readiness Team (TibCERT) busca criar uma estrutura formal e baseada em coalizão para reduzir e mitigar ameaças online na comunidade tibetana, bem como expandir a capacidade de pesquisa técnica sobre estas ameaças de vigilância e censura em nossa diáspora, garantindo maior liberdade e segurança online para a sociedade tibetana como um todo.

As missões do TibCERT incluem:

- Criar e manter uma plataforma para colaboração de longo prazo entre atores da comunidade tibetana para suas necessidades e questões de segurança digital
- Aprofundar as conexões e desenvolver um processo formal para colaboração entre pessoas tibetanas e pesquisadores globais de cibersegurança e malwares para garantir trocas que beneficiem a todes
- Ampliar os recursos disponíveis para defesa e mitigação de ataques digitais, publicizando regularmente informações e recomendações sobre ameaças vigentes na comunidade tibetana
- Auxiliar pessoas no Tibet a transpor a censura e a vigilância provendo informação frequente e detalhada, análises, e também possíveis soluções.
