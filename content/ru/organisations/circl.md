---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: рабочие часы (UTC+2)
response_time: 4 часа
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL — группа быстрого реагирования по цифровой безопасности для частного бизнеса, сообществ и НКО в Люксембурге.

Если вы пользователь, компания или организация из Люксембурга и находитесь под ударом, CIRCL может стать вашим надежным партнером. Работа
экспертов CIRCL похожа на действия бригады пожарных: они быстро и эффективно реагируют на угрозы и инциденты, как произошедшие, так и ожидаемые.

В задачи CIRCL входят систематический и оперативный сбор данных о цифровых угрозах, анализ, информирование сообщества, реагирование на инциденты.
