---
layout: page
title: E-mail
author: mfc, Metamorphosis Foundation
language: sq
summary: Metodat e kontaktimit
date: 2018-09
permalink: /sq/contact-methods/email.md
parent: /sq/
published: true
---

Përmbajtja e mesazhit tuaj si dhe fakti që keni kontaktuar me organizatën mund të jetë i arritshëm nga qeveritë apo organet e zbatimit të ligjit.
