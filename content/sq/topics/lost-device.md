---
layout: page
title: "Kam humbur pajisjen"
author: Hassen Selmi, Bahaa Nasr, Michael Carbone, bashkëpunëtorë të mëparshëm të DFAK-së, Metamorphosis Foundation
language: sq
summary: "Kam humbur pajisjen, çfarë duhet të bëj?"
date: 2019-03
permalink: /sq/topics/lost-device
parent: Home
---

# Kam humbur pajisjen

A e keni humbur pajisjen? A ua ka vjedhur apo marrë ndonjë palë e tretë?

Në këto situata, është e rëndësishme të ndërmerrni hapa të menjëhershëm për të zvogëluar rrezikun që dikush tjetër të hyjë në llogaritë, kontaktet dhe informacionin tuaj personal.

Kjo pjesë e Veglërisë së ndihmës së parë digjitale do t'ju njoftojë me disa çështje themelore, në mënyrë që të vlerësoni se si ta zvogëloni dëmin e mundshëm që lidhet me humbjen e një pajisje.

## Workflow

### question-1

A është pajisja ende e humbur?

 - [Po, ende është e humbur](#device-missing)
 - [Jo, më është kthyer](#device-returned)

### device-missing

> Është mirë të mendoni se çfarë mbrojtje të sigurisë kishte pajisja:
>
> * A ishte e mbrojtur hyrja në pajisje me fjalëkalim ose ndonjë masë tjetër të sigurisë?
> * A  është i aktivizuar enkriptimi i pajisjes?
> * Në çfarë gjendje ishte pajisja juaj kur humbi - A keni qenë i kyçur? A ishte pajisja e ndezur, por e mbyllur me fjalëkalim? A ishte në modin "sleep", "hibernate" apo i fikur plotësisht?

Duke pasur parasysh këto, mund të kuptoni më mirë se sa ka të ngjarë që dikush tjetër të ketë qasje në përmbajtjen në pajisjen tuaj.

- [Le të heqim qasjen e pajisjes në llogaritë e mia](#accounts)

### accounts

> Listoni të gjitha llogaritë në të cilat kishte qasje kjo pajisje. Ato mund të jenë llogari për e-mail, rrjete sociale, mesazhe, aplikacione për takime dhe aplikacione bankare, si dhe llogari që mund ta përdorin këtë pajisje për autentifikim sekondar.
>
> Për çdo llogari në të cilën pajisja juaj kishte qasje (të tilla si e-mail, rrjete sociale ose profile në internet), ju duhet ta hiqni autorizimin për këtë pajisje për ato llogari. Kjo mund të bëhet duke hyrë në llogaritë tuaja dhe duke e hequr pajisjen nga pajisjet e lejuara.
>
> * [Google Account](https://myaccount.google.com/device-activity)
> * [Facebook Account](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [iCloud Account](https://support.apple.com/en-us/HT205064)
> * [Twitter Account](https://twitter.com/settings/sessions)
> * [Yahoo Account](https://login.yahoo.com/account/activity)

Pasi të keni përfunduar heqjen e pajisjes nga llogaritë tuaja, le të sigurojmë fjalëkalimet tuaja që mund të kenë qenë në pajisje.

- [Mirë, le të merremi me fjalëkalimet](#passwords)

### passwords

> Mendoni për çdo fjalëkalim të ruajtur drejtpërdrejt në pajisje ose çdo shfletues ku janë ruajtur fjalëkalimet.
>
> Ndryshoni fjalëkalimet për të gjitha llogaritë në të cilat mund të hyhet nga kjo pajisje. Nëse nuk përdorni softuer për menaxhimin e fjalëkalimeve, konsideroni seriozisht përdorimin e një softueri të tillë për të krijuar dhe për të menaxhuar më mirë fjalëkalime të forta.

Pasi të keni ndryshuar fjalëkalimet për llogaritë në pajisjen tuaj, mendoni nëse keni përdorur ndonjë prej këtyre fjalëkalimeve për llogari të tjera - nëse po, ju lutemi ndryshoni edhe ato fjalëkalime.

- [Kam përdorur disa nga fjalëkalimet e mia në pajisjen e humbur për llogari të tjera](#same-password)
- [Të gjitha fjalëkalimet e mia që mund të jenë komprometuar ishin unike](#2fa)

### same-password

A e përdorni të njëjtin fjalëkalim në llogari ose pajisje të tjera përveç pajisjes së humbur? Nëse po, ndryshoni fjalëkalimin në ato llogari pasi ato gjithashtu mund të komprometohen.

- [Në rregull, i ndryshova të gjitha fjalëkalimet përkatëse](#2fa)

### 2fa

> Mundësimi i autentifikimit me dy faktorë në llogaritë për të cilat mendoni se mund të jenë në rrezik nga qasja e paautorizuar, do të zvogëlojë gjasat që dikush tjetër të ketë qasje në to.
>
> Aktivizoni autentifikimin me dy faktorë për të gjitha llogaritë në të cilat kishte qasje nga kjo pajisje. Ju lutemi vini re se jo të gjitha llogaritë mbështesin autentifikimin me 2 faktorë. Për një listë të shërbimeve dhe platformave që mbështesin autentifikimin me dy faktorë, ju lutemi vizitoni [Two Factor Auth](https://twofactorauth.org).

- [E kam mundësuar autentifikimin me dy faktorë në llogaritë e mia për t'i siguruar ato më tepër. Do të doja të përpiqesha të gjeja ose fshija pajisjen time](#find-erase-device)

### find-erase-device

>Mendoni se për çfarë e keni përdorur këtë pajisje - a ka informacion të ndjeshëm në këtë pajisje, të tilla si kontakte, vendndodhje ose mesazhe? A mundet që nëse këto të dhëna të zbulohen të jenë problematike për ju, punën tuaj apo të tjerët përveç vetes tuaj?
>
> Në disa raste mund të jetë e dobishme që të fshihen nga distanca të dhënat në pajisjen e një personi të arrestuar, për të parandaluar që të dhënat që gjenden në të të abuzohen dhe të përdoren kundër tyre ose aktivistëve të tjerë. Në të njëjtën kohë, kjo mund të jetë problematike për personin e arrestuar (veçanërisht në situatat kur torturimi dhe keqtrajtimi janë të mundshëm) - veçanërisht nëse personi i arrestuar është detyruar të mundësojë qasje në pajisje, të dhënat që zhduken papritmas nga pajisja mund t'i bëjnë autoritetet që kanë bërë arrestimin më dyshues. Nëse përballeni me një situatë të ngjashme, lexoni seksionin tonë ["Dikush që unë e njoh është arrestuar"](../../../../ arrestuar).
>
> Vlen gjithashtu të përmendet se në disa vende fshirja në distancë mund të jetë problematike në aspektin ligjor, pasi mund të interpretohet si shkatërrim i provave.
>
> Nëse jeni të vetëdijshëm për pasojat direkte dhe ligjore për të gjithë ata që janë të përfshirë në këtë situatë, mund të vazhdoni me përpjekjen për të fshirë pajisjen nga distanca, duke i ndjekur udhëzimet për sistemin tuaj operativ:
>
> * [pajijset Android](https://support.google.com/accounts/answer/6160491?hl=en)
> * [iPhone ose Mac](https://www.icloud.com/#find)
> * [pajisjet iOS (iPhone dhe iPad) duke përdorur iCloud](https://support.apple.com/kb/ph2701)
> * [pajisjet Windows](https://support.microsoft.com/en-us/help/11579/microsoft-account-find-and-lock-lost-windows-device)
> * [telefonat Blackberry](https://docs.blackberry.com/en/endpoint-management/blackberry-uem/12_9/blackberry-uem-self-service-user-guide/amo1375908155714)
> * Për pajisjet Windows ose Linux, ju mund të keni instaluar një softuer (të tillë si program kundër vjedhjeve ose antivirus) që ju lejon të fshini të dhënat nga distanca dhe historinë e pajisjes tuaj. Nëse është kështu, shfrytëzojeni këtë mundësi.

Pavarësisht nëse keni arritur ose jo të fshini nga distanca informacionin në pajisje, është ide e mirë t'i informoni kontaktet tuaja.

- [Vazhdoni në hapat e ardhshëm për të gjetur këshilla se si të informoni kontaktet tuaja se pajisja juaj është humbur.](#inform-network)

### inform-network

> Përveç llogarive tuaja, pajisja juaj ka të ngjarë të ketë informacion në lidhje me të tjerët. Kjo mund të përfshijë kontaktet tuaja, komunikimet me të tjerët, grupet për dërgimin e mesazheve.
>
> Kur merrni parasysh informimin e rrjetit dhe komunitetit tuaj në lidhje me pajisjen e humbur, përdorni [parimet e zvogëlimit të dëmit](../../../../arrested#harm-reduction) për të siguruar që duke i kontaktuar të tjerët, nuk i vendosni ato në rrezik më të madh.

Informoni rrjetin tuaj për pajisjen e humbur. Kjo mund të bëhet privatisht me kontaktet kryesore të rrezikut të lartë, ose duke postuar një liste të llogarive të komprometuara publikisht në ueb-faqen tuaj ose në llogarinë në rrjetin social, nëse mendoni se është e përshtatshme për ta bërë këtë.

- [I informova kontaktet e mia](#review-history)


### review-history

> Nëse është e mundur, rishikoni historinë e lidhjes/aktivitetet e të gjitha llogarive të lidhura me pajisjen. Kontrolloni për të parë nëse llogaria juaj është përdorur në kohën kur nuk keni qenë në internet ose nëse në llogarinë tuaj është hyrë nga një vend ose adresë IP e panjohur.
>
> Mund ta rishikoni veprimtarinë tuaj në disa ofrues të njohur të shërbimeve më poshtë:
>
> * [Google](https://myaccount.google.com/device-activity)
> * [Facebook](https://www.facebook.com/settings?tab=security&section=sessions&view)
> * [iCloud](https://support.apple.com/en-us/HT205064)
> * [Twitter](https://twitter.com/settings/sessions)
> * [Yahoo](https://login.yahoo.com/account/activity)

A e rishikuat historinë e lidhjes?

- [Po, dhe nuk gjeta asgjë të dyshimtë](#check-settings)
- [Po, dhe gjeta aktivitete të dyshimta](../../../account-access-issues/)

### check-settings

> Kontrolloni cilësimet e llogarisë të të gjitha llogarive të lidhura me pajisjen. A janë ndryshuar? Për llogaritë e e-mailit, kontrolloni cilësimet për përcjelljen automatike të e-maileve (auto-forward), ndryshimet e mundshme në adresën e e-mailit për back-up/resetim ose numrat e telefonit, sinkronizimin në pajisje të ndryshme, duke përfshirë telefonat, kompjuterët ose tabletët, dhe lejet e aplikacioneve ose lejet e tjera të llogarive.
>
> Përsëriteni rishikimin e historisë së aktivitetit të llogarisë të paktën një herë në javë për një muaj, për të siguruar se llogaria juaj të vazhdojë të tregojë se nuk ka asnjë aktivitet të dyshimtë.

- [Historia e aktivitetit të llogarisë sime tregon aktivitete të dyshimta](../../../account-access-issues/)
- [Historia e aktivitetit të llogarisë sime tregon ndonjë aktivitet të dyshimtë, megjithatë do të vazhdoj ta kontrolloj gjatë kohës](#resolved_end)


### device-returned

> Nëse pajisja juaj është humbur, është marrë nga një palë e tretë ose duhej të dorëzohej në një vendkalim kufitar, por u është kthyer përsëri, kini kujdes pasi nuk e dini se kush ka pasur qasje në të. Në varësi të nivelit të rrezikut me të cilin përballeni, mund të dëshironi ta trajtoni pajisjen sikur tani është e pabesueshme ose e komprometuar.

> Bëni vetes pyetjet e mëposhtme dhe vlerësoni rrezikun që pajisja juaj të jetë komprometuar:

> * Sa kohë ishte pajisja jashtë kontrollit tuaj?
> * Kush potencialisht mund të kishte qasje në të?
> * Pse do të donin të kenë qasje në të?
> * A ka shenja se pajisja është dëmtuar fizikisht?

> Nëse nuk i besoni më pajisjes tuaj, konsideroni të fshini dhe riinstaloni përsëri pajisjen ose të merrni një pajisje të re.

A dëshironi mbështetje për të marrë një pajisje tjetër?

- [Po](#new-device_end)
- [Jo](#resolved_end)


### accounts_end

Nëse keni humbur qasjen në llogaritë tuaja ose mendoni se dikush mund të ketë hyrë në llogaritë tuaja, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=account)

### new-device_end

Nëse keni nevojë për ndihmë financiare të jashtme për të marrë një pajisje tjetër, ju lutemi kontaktoni organizatat më poshtë që mund t'ju mbështesin.

:[](organisations?services=equipment_replacement)

### resolved_end

Shpresojmë që ky udhëzues i DFAK të ishte i dobishëm. Ju lutemi na jepni komentet tuaja [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Këtu keni një seri këshillash për të zbutur rrezikun e rrjedhjeve të të dhënave dhe qasjen e paautorizuar në llogaritë dhe informacionin tuaj:

- Asnjëherë mos e lini pajisjen tuaj të pa mbikëqyrur. Nëse nuk mund ta merrni me vete, mbylleni dhe ruajeni në një vend të sigurt.
- Aktivizoni enkriptimin e plotë të diskut.
- Përdorni një fjalëkalim të fortë për të kyçur pajisjen tuaj.
- Aktivizoni funksionin Gjej/Fshij telefonin tim kurdo që të jetë e mundur, por ju lutemi vini re se kjo mund të përdoret për t'ju ndjekur ose për ta fshirë pajisjen tuaj, nëse llogaria juaj e lidhur me pajisjen (Gmail/iCloud) është komprometuar.

#### resources

* Konsideroni të përdorni programe kundër vjedhjes si [Prey] (https://preyproject.com)
* [Siguria në një kuti për sigurimin e skedarëve (fajllave) të ndjeshëm](https://securityinabox.org/en/guide/secure-file-storage/)
* [Këshilla se si ta aktivizoni enkriptimin e plotë të diskut](https://communitydocs.accessnow.org/166-Full-Disk_Encryption.html)
