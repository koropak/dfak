---
layout: page
title: "Ueb-faqja ime ka rënë, çfarë po ndodh?"
author: Rarenet, Metamorphosis Foundation
language: sq
summary: "Një kërcënim me të cilin përballen shumë OJQ, media të pavarura dhe blogerë është që zëri i tye të heshtet sepse ueb-faqja e tyre ka rënë ose është hakuar."
date: 2018-09
permalink: /sq/topics/website-not-working/
parent: Home
---

# Ueb-faqja ime ka rënë, çfarë po ndodh?

Një kërcënim me të cilin përballen shumë OJQ, media të pavarura dhe blogerë është që zëri i tyre të heshtet sepse ueb-faqja e tyre ka rënë ose është hakuar. Ky është një problem zhgënjyes dhe mund të ketë shumë shkaqe si mirëmbajtja e keqe e ueb-faqes, hostingu jo i besueshëm, [script-kiddies](https://en.wikipedia.org/wiki/Script_kiddie), një sulm DDOS ose hakimi i ueb-faqes. Kjo pjesë e Veglërisë së ndihmës së parë digjitale do t'ju njoftojë me disa hapa themelorë për t'i diagnostikuar problemet e mundshme duke përdorur materiale nga [My Website is Down](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Është e rëndësishme të dini se ka shumë arsye pse ueb-faqja juaj mund të bjerë. Ato mund të jenë probleme teknike në kompaninë që e hoston ueb-faqen ose Content Management System (CMS) që nuk është azhurnuar, si Joomla ose Wordpress. Gjetja e problemit dhe zgjidhjeve të mundshme për problemin
e ueb-faqes suaj mund të jetë i rëndë. Është praktikë e mirë që **të kontaktoni web-masterin tuaj dhe hostin e ueb-faqes** pas diagnostikimit
të këtyre problemeve të zakonshme më poshtë. Nëse asnjë prej këtyre opsioneve nuk është në dispozicion për ju, [kërkoni ndihmë nga një organizatë që i besoni](../website-down_end).

## Konsideroni

Për fillim, konsideroni:

- Kush e zhvilloi ueb-faqen tuaj? A janë ata në dispozicion për të ndihmuar?
- A është zhvilluar duke përdorur Wordpress ose ndonjë platformë tjetër të popullarizuar CMS?
- Kush është ofruesi juaj i web-hosting-ut? Nëse nuk e dini, mund të përdorni një mjet [si ky] (http://www.whoishostingthis.com/) për t'ju ndihmuar.

## Workflow

### error-message

A shihni mesazhe gabimi?

 - [Po, jam duke parë mesazhe gabimi](#error-message-yes)
 - [Jo](#hosting-message)


### hosting-message

A shihni mesazh nga ofruesi juaj i web-hosting-ut?

- [Po, jam duke parë mesazh nga ofruesi im i hostingut](#hosting-message-yes)
- [Jo](#site-not-loading)


### site-not-loading

A nuk hapet aspak faqja juaj?

- [Po, faqja nuk hapet fare](#site-not-loading-yes)
- [Jo, ueb-faqja hapet](#hosting-working)


### hosting-working

A funksionon ueb-faqja e ofruesit tuaj të hostingut, por ueb-faqja juaj nuk hapet?

- [Po, mund ta hap ueb-faqen e ofruesit tim të hostingut](#hosting-working-yes)
- [Jo](#similar-content-censored)


### similar-content-censored

A jeni në gjendje të vizitoni faqe të tjera me përmbajtje të ngjashme si faqja juaj?

- [Nuk mund të vizitoj as faqe të tjera me përmbajtje të ngjashme.](#similar-content-censored-yes)
- [Faqet e tjera funksionojnë mirë. Unë thjesht nuk mund ta vizitoj faqen time](#loading-intermittently)


### loading-intermittently

A hapet faqja juaj kohë pas kohe (me ndërprerje), ose jashtëzakonisht ngadalë?

- [Po, faqja ime hapet kohë pas kohe (me ndërprerje) ose është e ngadaltë](#loading-intermittently-yes)
- [Jo, ueb-faqja ime hapet, por mund të jetë hakuar](#website-defaced)


### website-defaced

A hapet ueb-faqja juaj, por pamja dhe përmbajtja nuk janë ato që prisni të shihni?

- [Po, ueb-faqja ime nuk e ka përmbajtjen/pamjen e pritur](#defaced-attack-yes)
- [Jo](#website-down_end)


### error-message-yes

> Ky mund të jetë një ***problem i softuerit*** - duhet të mendoni për çdo ndryshim që ju ose ekipi juaj mund të keni bërë kohët e fundit, dhe të kontaktoni web-masterin tuaj. Po t'i dërgoni web-masterit një pamje nga ekrani, linku i faqes me të cilën keni probleme, dhe çdo mesazh gabimi që shihni do t'u ndihmojë atyre të kuptojnë se cili mund të jetë shkaku i problemit. Ju gjithashtu mund të kopjoni mesazhet e gabimit në një motor kërkimi për të parë nëse ka ndonjë mënyrë për rregullim të lehtë të problemit.

A ndihmoi kjo?

- [Po](#resolved_end)
- [Jo](#website-down_end)


### hosting-message-yes

> Ueb-faqja juaj mund të jetë hequr (offline) për arsye ligjore, [të drejtat e autorit](https://www.eff.org/issues/bloggers/legal/liability/IP), faturimi, ose arsye të tjera. Kontaktoni ofruesin tuaj të hostingut për detaje të mëtejshme se pse ata e kanë pezulluar hostingun e ueb-faqes suaj.

A ndihmoi kjo?

- [Po](#resolved_end)
- [Jo, kam nevojë për mbështetje ligjore](#legal_end)
- [Jo, kam nevojë për mbështetje teknike](#website-down_end)


### site-not-loading-yes

> Kompania juaj e hostingut mund të ketë probleme, dhe në këtë rast ju mund të përballeni me ***problem të hostingut***. A mund të vizitoni ueb-faqen e kompanisë suaj të hostingut? Vini re se kjo **nuk** është seksioni i administratorit të ueb-faqes suaj, por kompania ose organizata e cila e hoston faqen tuaj.
>
> Shikoni ose kërkoni blogun "status" (p.sh. status.dreamhost.com), e gjithashtu kërkoni në twitter.com përdoruesit e tjerë që diskutojnë për probleme të ngjashme në kompaninë e hostingut - një kërkim i thjeshtë si "(emri i kompanisë) down" shpesh mund të zbulojë nëse edhe shumë të tjerë e kanë të njëjtin problem.

A ndihmoi kjo?

- [Po](#resolved_end)
- [Jo, ueb-faqja e ofruesit tim të hsotingut nuk ka rënë - është funksionale](#hosting-working-yes)
- [Jo, kam nevojë për mbështetje teknike](#website-down_end)


### hosting-working-yes

> Kontrolloni URL-në tuaj në [këtë ueb-faqe](https://downforeveryoneorjustme.com/) - faqja juaj mund të jetë aktive, por ju nuk mund ta shihni.
>
>Nëse faqja juaj është në rregull, por ju nuk mund ta shihni, kjo me gjasë është ***problem i rrjetit***: lidhja juaj e internetit mund të ketë probleme ose mund ta bllokojë hyrjen tuaj në faqen tuaj.

Keni nevojë për ndihmë të mëtejshme?

- [Jo](#resolved_end)
- [Po, kam nevojë për ndihmë për të rivendosur lidhjen time të rrjetit](#website-down_end)
- [Po, ky nuk është problem i rrjetit dhe ueb-faqja ime nuk hapet për të gjithë](#similar-content-censored)


### similar-content-censored-yes

> Provoni të vizitoni ueb-faqe me përmbajtje të ngjashme me ueb-faqen tuaj. Provoni gjithashtu të përdorni [Tor](https://www.torproject.org/projects/gettor.html), [Psiphon](https://psiphon.ca/en/index.html), ose VPN për të hyrë në faqen tuaj.
>
> Nëse mund të vizitoni faqen tuaj përmes Tor, Psiphon ose VPN, ju keni ***problem censurimi*** - ju jeni akoma në internet për pjesët tjera të botës, por jeni duke u censuruar në vendin tuaj.

A dëshironi të bëni diçka në lidhje me këtë censurë?

- [Po, unë do të doja ta raportoja këtë publikisht dhe kam nevojë për mbështetje për fushatën time të avokimit](#advocacy_end)
- [Po, do të doja të gjeja një zgjidhje për ta bërë faqen time të qasshme](#website-down_end)
- [Jo](#resolved_end)


### loading-intermittently-yes

> Faqja juaj mund të jetë stërngarkuar nga numri dhe shpejtësia e kërkesave për qasje në të në një kohë të shkurtër - ky është problem i ***performancës*** .
>
> Kjo mund të jetë "e mirë" për shkak se faqja juaj është bërë më e popullarizuar dhe thjesht duhen disa përmirësime që t'iu përgjigjur sa më shumë lexuesve - kontrolloni analizat e faqes suaj për një model afatgjatë të rritjes. Kontaktoni web-masterin tuaj ose ofruesin e hostingut. Shumë platforma të njohura të blogjeve dhe CMS (Joomla, Wordpress, Drupal ...) kanë shtojca për të ndihmuar në "cache"-imin e faqes suaj lokalisht dhe integrimin [CDN] (https://en.wikipedia.org/wiki/Content_delivery_network), që mund ta përmirësojnë ndjeshëm performancën dhe qëndrueshmërinë e faqes. Shumë prej zgjidhjeve më poshtë mund të ndihmojnë gjithashtu sa i përket problemeve të performancës.
>
> Nëse jeni duke u përballur me një problem të rëndë **të performancës**, faqja juaj mund të jetë viktimë e një [**sulmi "distributed denial of service"**](https://ssd.eff.org/en/glossary/distributed-denial-service-attack) (ose DDoS). Ndiqni hapat më poshtë për të zbutur një sulm të tillë:
>
> - Hapi 1: Kontaktoni një person të besuar i cili mund të ndihmojë me ueb-faqen tuaj (web-masteri juaj, njerëzit që u kanë ndihmuar ta krijoni faqen tuaj, punonjësit tuaj të brendshëm ose ofruesi juaj të hostingut).
>
> - Hapi 2: Kontaktoni kompaninë ku e keni blerë emrin e domenit dhe ndryshoni "Time to Live" ose TTL në 1 orë (mund të gjeni udhëzime se si ta bëni këtë në faqet e shumë ofruesve të hostingut, si [Network Solutions](http://www.networksolutions.com/support/how-to-manage-advanced-dns-records/) ose [GoDaddy](http://support.godaddy.com/help/article/680/managing-dns-for-your-domain-names)). Kjo mund t'ju ndihmojë të ridrejtoni faqen tuaj shumë më shpejt pasi të jetë nën sulm (parazgjedhja është 72 orë, ose tre ditë). Ky cilësim shpesh do të gjendet në vetitë "advanced" të domenit tuaj, e ndonjëherë pjesë e SRV ose Service records.
>
> - Hapi 3: Zhvendosni faqen tuaj në një shërbim për zbutjen e sulmeve DDoS. Filloni me:
>
>     - [Deflect.ca](https://deflect.ca/)
>     - [Google's Project Shield](https://projectshield.withgoogle.com/landing)
>     - [CloudFlare's Project Galileo](https://www.cloudflare.com/galileo)
>
> Për një listë të plotë të organizatave të besueshme që mund të ndihmojnë në zbutjen e një sulmi DDoS, shihni [këtë pjesë](../ddos_end).
>
> - Hapi 4: Sapo të keni rimarrë kontrollin, rishikoni nevojat tuaja dhe vendosni midis një ofruesi të sigurt të hostingut ose thjesht të vazhdoni me shërbimin tuaj për zbutjen e sulmeve DDoS.

- Për një listë të plotë të organizatave të besueshme që mund të ofrojnë hosting të sigurt, shikoni [këtë seksion](#web-hosting_end).

### defaced-attack-yes

> Shpërfytyrimi i ueb-faqes është një praktikë ku një sulmues e zëvendëson përmbajtjen ose pamjen vizuale të ueb-faqes me përmbajtjen e tij. Këto sulme zakonisht kryhen ose duke shfrytëzuar dobësitë në platforma të pa mirëmbajtura të CMS, pa azhurnimet më të fundit të sigurisë ose duke përdorur emrat e përdoruesve/fjalëkalimet e vjedhura të llogarisë së hostingut.
>
> - Hapi 1: Verifikoni që kjo është një marrje keqdashëse përsipër e ueb-faqes suaj. Një praktikë fatkeqe, por ligjore, është që të blini emrat e domeneve që kanë skaduar së fundmi për të "marrë përsipër" trafikun që ata kishin për qëllime reklamimi. Është shumë e rëndësishme të bëni në rregull/ në kohë pagesat për emrin e domenit.
> - Hapi 2: Nëse ueb-faqja juaj është shpërfytyruar, së pari rivendosni kontrollin e llogarisë tuaj të hyrjes në ueb-faqen tuaj dhe rivendosni fjalëkalimin e saj, shihni seksionin [Kidnapimi i llogarisë](../../../account-access-issues) për ndihmë.
> - Hapi 3: Bëni një kopje rezervë të ueb-faqes së shpërfytyruar, që mund të përdoret më vonë për hetimin e shpërfytyrimit.
> - Hapi 4: Shkyçeni përkohësisht ueb-faqen tuaj - përdorni një faqe të thjeshtë për të informuar se jeni duke "punuar në rregullimin e faqes".
> - Hapi 5: Përcaktoni se si u hakua faqja juaj. Ofruesi juaj i hostingut mund të jetë në gjendje të ndihmojë. Problemet e zakonshme janë pjesë më të vjetra të faqes tuaj me skripte/mjete të personalizuara që ekzekutohen në to, sisteme të vjetra të administrimit të përmbajtjes dhe programim sipas kërkesës me të meta të sigurisë.
> - Hapi 6: Rivendosni ueb-faqen tuaj origjinale nga kopjet rezervë. Nëse as ju, e as kompania juaj e hostingut nuk ka kopje rezervë, mund t'ju duhet të rindërtoni ueb-faqen tuaj nga e para! Vini re gjithashtu që nëse kopjet rezervë të vetme gjendet tek ofruesi juaj i hostingut, sulmuesi mund të jetë në gjendje t'i fshijë ato kur ta marrë kontrollin e faqes suaj!

A u ndihmuan këto rekomandime?

- [Po](#resolved_end)
- [Jo](#website-down_end)


### website-down_end

> Nëse akoma keni nevojë për ndihmë pas të gjitha pyetjeve që ju janë përgjigjur, mund të kontaktoni një organizatë të besuar dhe të kërkoni mbështetje.
>
> Para se t'i kontaktoni, ju lutemi bëni vetes pyetjet e mëposhtme:
>
> - Si është e strukturuar dhe si mirëmbahet kompania/organizata? Cilat lloje të verifikimit ose raportimit kërkohen t'i bëjnë ato, nëse bëjnë?
> - Konsideroni se në cilin vend/shtet ata kanë prani ligjore,  zbatimin e ligjeve dhe kërkesat e tjera ligjore të të cilit ato duhet t'i respektojnë.
> - Cilat regjistra për mbajtje të shënimeve krijohen dhe për sa kohë janë në dispozicion?
> - A ka kufizime në lidhje me llojin e përmbajtjes që shërbimi do ta hostojë/përcjellë dhe a mund të kenë ato ndikim në faqen tuaj?
> - A ka kufizime se në cila vende ata mund t'i ofrojnë shërbimet e tyre?
> - A pranojnë ata një formë pagese që mund të përdorni? A mund ta përballoni shërbimin e tyre?
> - Komunikimet e sigurta - duhet të jeni në gjendje të hyni në mënyrë të sigurt dhe të komunikoni me ofruesin e shërbimit privatisht.
> - A ekziston mundësi për autentifikimin me dy faktorë, për të përmirësuar sigurinë e qasjes së administratorit? Kjo ose politikat e ngjashme për qasje të sigurt mund të ndihmojë në zvogëlimin e kërcënimit nga format e tjera të sulmeve kundër ueb-faqes suaj.
> - Në cilin lloj mbështetjeje të vazhdueshme do të keni qasje? A ka ndonjë kosto shtesë për mbështetje, dhe/ose a do të merrni mbështetje të mjaftueshme nëse përdorni nivelin 'falas' të mbështetjes?
> - A mund ta 'testoni' ueb-faqen tuaj në version testues te hosti përpara se ta bëni publike?

Këtu është një listë e organizatave që mund të ndihmojnë në çështjen tuaj:

:[](organisations?services=web_protection)

### legal_end

> Nëse ueb-faqja juaj ka rënë për shkak të arsyeve ligjore dhe keni nevojë për mbështetje ligjore, ju lutemi kontaktoni një organizatë që mund të ndihmojë:

:[](organisations?services=legal)

### advocacy_end

> Nëse dëshironi mbështetje për të filluar një fushatë kundër censurës, ju lutemi kontaktoni organizatat që mund të ndihmojnë në përpjekjet e avokimit:

:[](organisations?services=advocacy)

### ddos_end

> Nëse keni nevojë për ndihmë për të zbutur një sulm DDoS kundër ueb-faqes suaj, ju lutemi kontaktoni organizatat e specializuara në zbutjen e sulmeve DDoS:

:[](organisations?services=ddos)


### web-hosting_end

> Nëse jeni duke kërkuar një organizatë të besuar për ta hostuar ueb-faqen tuaj në një server të sigurt, ju lutemi shikoni listën më poshtë.
>
> Para se të kontaktoni me këto organizata, ju lutemi mendoni për këto pyetje:
>
> - A ofrojnë ata mbështetje të plotë për zhvendosjen e faqes suaj në shërbimin e tyre?
> - A janë shërbimet të barabarta ose më të mira se hosti juaj aktual, të paktën për mjetet/shërbimet që përdorni? Gjërat kryesore që duhet të kontrolloni janë:
>     - Pultet e menaxhimit si cPanel
>     - Llogaritë e e-maileve (sa, kuotat, qasja përmes SMTP, IMAP)
>     - Bazat e të dhënave (sa, llojet, qasja)
>     - Qasja në distancë përmes SFTP/SSH
>     - Mbështetje për gjuhën e programimit (PHP, Perl, Ruby, cgi-bin access...) ose CMS (Drupal, Joomla, Wordpress…) që e përdor faqja juaj.

Këtu është një listë e organizatave që mund t'ju ndihmojnë me hostimin e faqes në internet:

:[](organisations?services=web_hosting)


### resolved_end

Shpresojmë që DFAK të ishte e dobishëm. Ju lutemi na jepni komentet tuaja (përmes e-mailit) (mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Kopjet rezervë** - Përveç shërbimeve dhe sugjerimeve më poshtë, është gjithmonë ide e mirë të siguroheni se keni kopje rezervë (që i ruani diku tjetër nga vendi ku është ueb-faqja juaj!). Shumë hostë dhe platforma të ueb-faqeve e përfshijnë këtë, por është mirë që të keni kopje shtesë, off-line.

- ** Azhurnoni softuerin rregullisht ** - Nëse jeni duke përdorur një Sistem të menaxhimit të përmbajtjes (CMS) si WordPress ose Drupal, sigurohuni që teknologjia e ueb-faqes suaj të jetë e azhurnuar me softuerin më të fundit, veçanërisht nëse ka pasur azhurnime të sigurisë.

- **Monitorimi** -Ka shumë shërbime që mund të kontrollojnë vazhdimisht në ueb-faqen dhe e-mail-in tuaj ose 'u dërgojnë mesazh nëse ato nuk  janë funksionale. [Ky artikull i Mashable](http://mashable.com/2010/04/09/free-uptime-monitoring/) ofron një listë prej 10 shërbimeve më të popullarizuara. Jini të vetëdijshëm që e-maili ose numri i telefonit që e përdorni për monitorim do të lidhet qartë me menaxhimin e ueb-faqes.

#### resources

- [Ueb-faqja ime ka rënë (nuk është funksionale)](https://github.com/OpenInternet/MyWebsiteIsDown)
- [Mirëmbajta funksionale e ueb-faqes suaj ](https://www.eff.org/keeping-your-site-alive)
- [Masat proaktive dhe reaktive kundër sulmeve DDoS](https://www.cert.be/en/paper/ddos-proactive-and-reactive-measures)
